package ru.an_sklad.ru;

import static java.security.AccessController.getContext;
import static ru.an_sklad.ru.SyncActivity.baseView;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import ru.an_sklad.ru.ui.scn.ListStateAdapter;
import ru.an_sklad.ru.ui.scn.State;


public class ScnActivity extends AppCompatActivity {
    ArrayList<String[]> listDataV = new ArrayList<String[]>();
    SQLiteDatabase db;
    ArrayList<State> states = new ArrayList<State>();
    ListView countriesList;
    EditText editTextNumber;
    TextView textViewVName, textViewVCode, textViewBarcodes, textViewPlaceName, textViewZonaVal,
            textViewKolvo, textViewMestoId, textViewEmpty, textViewProgressBar, textViewCopyBB;
    InputMethodManager imm;
    public static boolean listUpdate;
    private ListStateAdapter listStateAdapter;
    String globalUrl = MainActivity.getStatusUrl();
    String kodtov = "";
    StringBuilder scanKodOut = new StringBuilder();
    String place_name = "";
    String kolvo = "";
    String tov_name = "";
    String barcode = "";
    Integer countRepeatBar = 1;
    ProgressBar progressBarWait;
    ImageButton imageButtonCopyBB;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        db = getApplicationContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);
        String kodTov = getIntent().getStringExtra("kodTov");
        setContentView(R.layout.fragment_scn);

        textViewEmpty = (TextView) findViewById(R.id.textViewEmpty);
        textViewCopyBB = (TextView) findViewById(R.id.textViewCopyBB);
        textViewVName = (TextView) findViewById(R.id.textViewVName);
        textViewVCode = (TextView) findViewById(R.id.textViewVCode);
        textViewBarcodes = (TextView) findViewById(R.id.textViewBarcodes);
        textViewPlaceName = (TextView) findViewById(R.id.textViewPlaceName);
        textViewZonaVal = (TextView) findViewById(R.id.textViewZonaVal);
        textViewKolvo = (TextView) findViewById(R.id.textViewKolvo);
        textViewMestoId = (TextView) findViewById(R.id.textViewMestoId);
        progressBarWait = (ProgressBar) findViewById(R.id.progressBarWait);
        textViewProgressBar = (TextView) findViewById(R.id.textViewProgressBar);
        imageButtonCopyBB = (ImageButton) findViewById(R.id.imageButtonCopyBB);
        baseView = this.findViewById(R.id.constraintLayout);

        if(MainActivity.getStatusUserAdmin(db)) {
            listStateAdapter = new ListStateAdapter(getApplicationContext(), R.layout.list_item_scn, states);
        }else{
            listStateAdapter = new ListStateAdapter(getApplicationContext(), R.layout.list_item_scn_user, states);
        }

        countriesList = findViewById(R.id.countriesListPool);
        countriesList.setAdapter(listStateAdapter);

        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        editTextNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        String s = editTextNumber.getText().toString();
                        if(!String.valueOf(s.length()).equals("0")) {
                            hideSoftKeyboard(editTextNumber, getApplicationContext());
                            editTextNumber.setText("");
                            getSqlDataFSCN(s);
                        }else{
                            Global.ToastMsg("Нет данных для поиска", "error");
                        }
                    return true;
                }
                return false;
            }
        });

        if (kodTov != null){
            getSqlDataFSCN(kodTov);
            textViewEmpty.setVisibility(View.GONE);
        }
        editTextNumber.requestFocus();

        imageButtonCopyBB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String barcode = textViewBarcodes.getText().toString().split(",")[0];
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                if(barcode.length() > 0){
                    ClipData clip = ClipData.newPlainText("barcode", barcode);
                    clipboard.setPrimaryClip(clip);
                    Global.ToastMsg("Скопирован ШК: " + barcode, "done");
                }
            }
        });
    }

    private void getSqlDataFSCNOnline(String kodtov) {
        progressBarWait.setVisibility(View.VISIBLE);

        String url = globalUrl + "get_kodtovfind";

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("kodtov", kodtov);
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                . writeTimeout(5, TimeUnit.SECONDS)
                .build();

        AndroidNetworking.post(url)
                .addJSONObjectBody(jsonBody)
                .setPriority(Priority.HIGH)
                .setOkHttpClient(okHttpClient)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray response_pool = response.getJSONArray("kodtov");

                            for (int i = 0; i < Objects.requireNonNull(response_pool).length(); i++) {
                                try {
                                    JSONObject json_data = response_pool.getJSONObject(i);

                                    for (int i2 = 0; i2 < json_data.length(); i2++) {
                                        try {
                                            JSONObject json_data2 = json_data.getJSONObject(String.valueOf(i));
                                            String mesto_id = json_data2.getString("mesto_id");
                                            String zona_id = json_data2.getString("zona_id");
                                            String placename = json_data2.getString("placename");
                                            String kolvo = json_data2.getString("kolvo");
                                            listDataV.add(i2, new String[]{scanKodOut.toString(), "",  "", placename, kolvo, tov_name, zona_id, mesto_id});

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setListR();
                            listStateAdapter.getAllList();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBarWait.setVisibility(View.GONE);
                        textViewProgressBar.setVisibility(View.GONE);
                        countRepeatBar = 1;
                    }
                    @Override
                    public void onError(ANError error) {
                        countRepeatBar = countRepeatBar + 1;
                        textViewProgressBar.setText(countRepeatBar.toString());
                        textViewProgressBar.setTextColor(Color.RED);
                        textViewProgressBar.setVisibility(View.VISIBLE);
                        getSqlDataFSCNOnline(kodtov);
                    }
                });

        setSqlDataName(kodtov);
        setListR();
        listStateAdapter.getAllList();
    }

    public static void hideSoftKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.clearFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEtSearch.getWindowToken(), 0);
    }

    private void getSqlDataFSCN(String kod) {
        scanKodOut.setLength(0);
        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);
        listDataV.clear();
        states.clear();
        textViewVCode.setText("");
        textViewVName.setText("");

        String sql2 = "SELECT scankod " +
               "FROM scancodes " +
               "where " +
               "scancodes.kodtov = (SELECT kodtov FROM scancodes WHERE scankod = '" + kod + "') " +
               "or scancodes.kodtov = (SELECT kodtov FROM kodtov WHERE LOWER(artikul) = '" + kod.toLowerCase(Locale.ROOT) + "') " +
               "or scancodes.kodtov = '" + kod + "' ";

        Cursor query2 = db.rawQuery(sql2, null);

        if (query2 != null) {
            if (query2.getCount() != 0) {
                if (query2.moveToFirst()) {
                    int i = 0;
                    do {
                        scanKodOut.append(query2.getString(0) + ", ");
                    } while (query2.moveToNext());
                    query2.close();
                }
            }
        }

        String sql =  "SELECT scancodes.kodtov, stock.place_name, stock.kolvo, kodtov.tov_name " +
                "FROM scancodes " +
                "LEFT JOIN stock on stock.kodtov = scancodes.kodtov " +
                "LEFT JOIN kodtov on kodtov.kodtov = scancodes.kodtov " +
                "where scancodes.kodtov = (SELECT kodtov FROM scancodes WHERE scankod = '" + kod + "') " +
                "OR scancodes.kodtov = '" + kod + "' " +
                "or scancodes.kodtov = (SELECT kodtov FROM kodtov WHERE LOWER(artikul) = '" + kod.toLowerCase(Locale.ROOT) + "') " +
                "LIMIT 1 ";

        Cursor query = db.rawQuery(sql, null);

        if (query != null) {
            if (query.getCount() != 0) {
                if (query.moveToFirst()) {
                    int i = 0;
                    do {
                        kodtov = query.getString(0);
                        place_name = query.getString(1);
                        kolvo = query.getString(2);
                        tov_name = query.getString(3);
                        i = +1;
                    } while (query.moveToNext());
                    query.close();
                }
                textViewEmpty.setVisibility(View.GONE);
                textViewPlaceName.setVisibility(View.VISIBLE);
                textViewMestoId.setVisibility(View.VISIBLE);
                textViewZonaVal.setVisibility(View.VISIBLE);
                textViewKolvo.setVisibility(View.VISIBLE);
                countriesList.setVisibility(View.VISIBLE);
                imageButtonCopyBB.setVisibility(View.VISIBLE);
                textViewCopyBB.setVisibility(View.VISIBLE);

                barcode = kod;
                setSqlDataName(kodtov);
                getSqlDataFSCNOnline(kodtov);

                editTextNumber.setText("");

            }else{
                listDataV.clear();
                textViewVName.setText("");
                textViewVCode.setText("");
                textViewBarcodes.setText("");

                textViewEmpty.setVisibility(View.VISIBLE);
                textViewPlaceName.setVisibility(View.INVISIBLE);
                textViewMestoId.setVisibility(View.INVISIBLE);
                textViewZonaVal.setVisibility(View.INVISIBLE);
                textViewKolvo.setVisibility(View.INVISIBLE);
                countriesList.setVisibility(View.INVISIBLE);
                imageButtonCopyBB.setVisibility(View.INVISIBLE);
                textViewCopyBB.setVisibility(View.INVISIBLE);
                listStateAdapter.notifyDataSetChanged();
                Global.ToastMsg("Не найдено", "error");
            }
        }
    }

    private void getSqlDataSCN(String kodTov){
        if (kodTov == null){
            return;
        }

        String sql = "" +
                "SELECT scankod " +
                "FROM scancodes " +
                "where kodtov = '" + kodTov + "' ";

        Cursor query = db.rawQuery(sql, null);
        if (query != null) {
            if (query.moveToFirst()) {
                int i = 0;
                do {
                    String scankod = query.getString(0);
                    listDataV.add(i, new String[]{scankod, ""});
                    i =+1;
                } while (query.moveToNext());
                setSqlDataName(kodTov);
                query.close();
            }
        }
    }

    private void setSqlDataName(String kodTov){
        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);
        String sql = "" +
                "SELECT tov_name, artikul " +
                "FROM kodtov " +
                "where kodtov = '" + kodTov + "' ";

        Cursor query = db.rawQuery(sql, null);
        if (query != null) {
            if (query.moveToFirst()) {
                String tov_name = query.getString(0);
                String artikul = query.getString(1);
                textViewVName.setText(tov_name);
                textViewVCode.setText(artikul);
                query.close();
            }
            db.close();
        }
        String s = removeLastCharsPlace(scanKodOut.toString());
        textViewBarcodes.setText(s);
    }

    public static String removeLastCharsPlace(String s) {
        return (s == null || s.length() == 0)
                ? null
                : (s.substring(0, s.length() - 2));
    }

    @SuppressLint("SetTextI18n")
    private void setListR() {
        for (String[] strings : listDataV) {
            states.add(new State(strings[0], strings[1], strings[2], strings[3], strings[4], strings[5], strings[6], strings[7]));
        }
    }

    public void ClearEditTextNumber(View view) {
        editTextNumber.setText("");
        editTextNumber.requestFocus();
        listStateAdapter.getAllList();
    }
};

