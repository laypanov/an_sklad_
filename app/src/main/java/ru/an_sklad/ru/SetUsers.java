package ru.an_sklad.ru;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SetUsers extends AppCompatActivity {
    SQLiteDatabase db;
    String selectionTextViewA = "";

    String employ_id1;
    String employ_id2;
    String user_admin1;
    String user_admin2;
    String[] mydata;
    String pin_code_isp1;
    String pin_code_isp2;


    private EditText PswIsp1;
    private EditText PswIsp2;
    private CheckBox checkBoxIsp1;
    private CheckBox checkBoxIsp2;
    private ImageButton imageButtonIsp1;
    private ImageButton imageButtonIsp2;
    private AutoCompleteTextView autoCompleteTextViewIsp1;
    private AutoCompleteTextView autoCompleteTextViewIsp2;
    String select_head_id;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_setusers);
        db = getBaseContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        PswIsp1 = (EditText) findViewById(R.id.editTextNumberPasswordIsp1);
        PswIsp2 = (EditText) findViewById(R.id.editTextNumberPasswordIsp2);
        checkBoxIsp1 = (CheckBox) findViewById(R.id.checkBoxIsp1);
        checkBoxIsp2 = (CheckBox) findViewById(R.id.checkBoxIsp2);
        imageButtonIsp1 = (ImageButton) findViewById(R.id.imageButtonIsp1);
        imageButtonIsp2 = (ImageButton) findViewById(R.id.imageButtonIsp2);
        autoCompleteTextViewIsp1 = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewIsp1);
        autoCompleteTextViewIsp2 = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewIsp2);
        PswIsp1.addTextChangedListener(PswIsp1Watcher);
        PswIsp2.addTextChangedListener(PswIsp2Watcher);

        select_head_id = getIntent().getStringExtra("select_head_id");

        if (autoCompleteTextViewIsp1.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

        ArrayList<String> array = new ArrayList<>();
        Cursor query = db.rawQuery("SELECT employ_id, employ_full_name, job_name  FROM users", null);
        if (query != null) {
            mydata = new String[query.getCount()];
            if (query.moveToFirst()) {
                int i = 0;
                do {
                    mydata[i] = query.getString(1) + " (" + query.getString(2) + ") " + "[" + query.getString(0) + "]";
                    i++;
                } while (query.moveToNext());
                query.close();
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, mydata);

        autoCompleteTextViewIsp1.setAdapter(adapter);
        autoCompleteTextViewIsp2.setAdapter(adapter);

        autoCompleteTextViewIsp1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {

                String employ_id = autoCompleteTextViewIsp1.getEditableText().toString();
                employ_id = employ_id.split("\\[")[1];
                employ_id = employ_id.split("]")[0];

                Cursor query = db.rawQuery("SELECT pin_code, is_admin FROM users WHERE employ_id = '" + employ_id + "'", null);
                if (query != null) {
                    if (query.moveToFirst()) {
                        pin_code_isp1 = query.getString(0);
                        user_admin1 = query.getString(1);
                    }
                    query.close();
                } else {
                    checkBoxIsp1.setVisibility(View.VISIBLE);
                    checkBoxIsp1.isChecked();
                }
                if (user_admin1.equals("True")) {
                    PswIsp1.setVisibility(View.VISIBLE);
                    PswIsp1.requestFocus();

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(PswIsp1, InputMethodManager.SHOW_IMPLICIT);
                }else{
                    autoCompleteTextViewIsp2.setVisibility(View.VISIBLE);
                    autoCompleteTextViewIsp2.requestFocus();
                }

                autoCompleteTextViewIsp1.setEnabled(false);
                imageButtonIsp1.setVisibility(View.VISIBLE);
                employ_id1 = employ_id;
            }
        });
        autoCompleteTextViewIsp2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {

                String employ_id = autoCompleteTextViewIsp2.getEditableText().toString();
                employ_id = employ_id.split("\\[")[1];
                employ_id = employ_id.split("]")[0];

                Cursor query = db.rawQuery("SELECT pin_code, is_admin FROM users WHERE employ_id = '" + employ_id + "'", null);
                if (query != null) {
                    if (query.moveToFirst()) {
                        pin_code_isp2 = query.getString(0);
                        user_admin2 = query.getString(1);
                    }
                    query.close();
                } else {
                    checkBoxIsp2.setVisibility(View.VISIBLE);
                    checkBoxIsp2.isChecked();
                }
                if (user_admin2.equals("True")) {
                    PswIsp2.setVisibility(View.VISIBLE);
                    PswIsp2.requestFocus();

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(PswIsp2, InputMethodManager.SHOW_IMPLICIT);
                }
                autoCompleteTextViewIsp2.setEnabled(false);
                imageButtonIsp2.setVisibility(View.VISIBLE);
                employ_id2 = employ_id;
            }
        });
    }

    @Override
    public void onBackPressed() {
        return;
    }

    private TextWatcher  PswIsp1Watcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {}
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        public void onTextChanged(CharSequence s, int start, int before,int count) {
            if (start > 0) {
                checkPpinIsp1();
            }
        }
    };

    private TextWatcher  PswIsp2Watcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {}
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (start > 0) {
                checkPpinIsp2();
            }
        }
    };

    private void checkPpinIsp1(){
        String textIsp1 = PswIsp1.getEditableText().toString();
        if (textIsp1.equals(pin_code_isp1)) {
            checkBoxIsp1.setVisibility(View.VISIBLE);
            imageButtonIsp1.setVisibility(View.VISIBLE);
            checkBoxIsp1.setChecked(true);
            PswIsp1.setEnabled(false);
            autoCompleteTextViewIsp1.setEnabled(false);
            autoCompleteTextViewIsp2.setVisibility(View.VISIBLE);
        }
    };

    private void checkPpinIsp2(){
        String textIsp2 = PswIsp2.getEditableText().toString();
        if (textIsp2.equals(pin_code_isp2)) {
            checkBoxIsp2.setVisibility(View.VISIBLE);
            imageButtonIsp2.setVisibility(View.VISIBLE);
            checkBoxIsp2.setChecked(true);
            PswIsp2.setEnabled(false);
            autoCompleteTextViewIsp2.setEnabled(false);
        }
    };

    public void onClickImageButtonIsp1(View view) {
        checkBoxIsp1.setChecked(false);
        PswIsp1.setEnabled(true);
        autoCompleteTextViewIsp1.setEnabled(true);
        checkBoxIsp1.setVisibility(View.GONE);
        imageButtonIsp1.setVisibility(View.GONE);
        PswIsp1.setVisibility(View.GONE);
        autoCompleteTextViewIsp1.setText("");
        PswIsp1.setText("");
        autoCompleteTextViewIsp1.requestFocus();
        if (checkBoxIsp2.isChecked()) {
        } else {
            autoCompleteTextViewIsp2.setVisibility(View.INVISIBLE);
        }
    }
    public void onClickImageButtonIsp2(View view) {
        checkBoxIsp2.setChecked(false);
        PswIsp2.setEnabled(true);
        autoCompleteTextViewIsp2.setEnabled(true);
        checkBoxIsp2.setVisibility(View.GONE);
        imageButtonIsp2.setVisibility(View.GONE);
        PswIsp2.setVisibility(View.GONE);
        autoCompleteTextViewIsp2.setText("");
        PswIsp2.setText("");
        autoCompleteTextViewIsp2.requestFocus();
    }

    public void bnext(View view) {

        db.execSQL("INSERT INTO common (employ_id, user_admin, head_id) VALUES ('" + employ_id1 + "', '" + user_admin1 + "', '" + select_head_id + "');");
        if (employ_id2 != null){
            db.execSQL("INSERT INTO common (employ_id, user_admin, head_id) VALUES ('" + employ_id2 + "', '" + user_admin2 + "', '" + select_head_id + "');");
        }

        Intent intent = new Intent(this, Main1Activity.class);
        startActivity(intent);
        finish();
    }
}
