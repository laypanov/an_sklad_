package ru.an_sklad.ru;

import static ru.an_sklad.ru.SyncActivity.baseView;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import ru.an_sklad.ru.ui.shelfcheck2.ListStateAdapter;
import ru.an_sklad.ru.ui.shelfcheck2.State;


public class ShelfCheck2Activity extends AppCompatActivity {
    public static boolean listUpdate;
    private static ArrayList<String[][][]> listDataR = new ArrayList<String[][][]>();
    private static String[][][] listDataR2;
    // private static String[][] listDataV;
    ArrayList<String[]> listDataV = new ArrayList<>();
    ArrayList<State> states = new ArrayList<State>();
    ArrayList<String> compliteListDataV = new ArrayList<String>();
    ListView countriesList;
    TabLayout tabLayout;
    Button button3, buttonShowRV, buttonShowRI, buttonSave, buttonClose;
    ImageButton imageButtonBarcode, imageButtonNullItem;
    EditText editTextCalcResult, editTextNumber;
    TextView textViewResult, textViewError, textViewPolka, textViewProgressBar;
    ProgressBar progressBarWait;
    ViewPager2 pager;
    private int lastPosImage;
    EditText  editTextCountFact;
    InputMethodManager imm;
    private ListStateAdapter listStateAdapter;
    public static String shortName;
    String recId;
    public static  Integer countDoc;
    private SQLiteDatabase db;
    Spinner spinnerHeads;
    String select_head_id;
    List<String> categories = new ArrayList<String>();
    String globalUrl = MainActivity.getStatusUrl();
    Integer countRepeatBar = 1;
    String kodTov, head_id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = getApplicationContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.fragment_shelf_check2);

        kodTov = getIntent().getStringExtra("kodTov");
        head_id = getIntent().getStringExtra("head_id");

        getSupportActionBar().hide();


        baseView = this.findViewById(R.id.constraintLayout);

        listStateAdapter = new ListStateAdapter(getApplicationContext(), R.layout.list_item_shelf_check2, states);
        countriesList = findViewById(R.id.countriesListShelfCheck2);
        countriesList.setAdapter(listStateAdapter);
        countriesList.setScrollingCacheEnabled(false);
        selectDocs();

    }

    private void setInitialData() {
        listStateAdapter.getClear();
        states.clear();

        for (String[] strings : listDataV) {
            states.add(new State(strings[0], strings[1], strings[2], strings[3], strings[4], strings[5], strings[6]));
        }

        listDataV.clear();
        listStateAdapter.getAllList();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }


    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    private void selectDocs(){
        Cursor query = db.rawQuery("" +
                "select  docplace.short_namex, docplace.user1, docplace.user2, STRFTIME('%Y-%m-%d %H:%M', timestamp), " +
                "           docplace_main.kolvo_fact, docplace_main.kolvo_base, users.employ_name " +
                "from docplace, docplace_main,users  " +
                "where  " +
                "docplace_main.kodtov = '" + kodTov + "' " +
                "and head_id = '" + head_id + "' and status = 4 " +
                "and docplace_main.shelf_uid = docplace.shelf_uid " +
                "and users.employ_id = docplace.user1 " +
                "group by docplace_main.shelf_uid " +
                "order by timestamp", null);
        if (query != null) {
            if (query.moveToFirst()) {
                int i = 0;

                do {
                    String short_namex = query.getString(0);
                    String user1 = query.getString(1);
                    String user2 = query.getString(2);
                    String timestamp = query.getString(3);
                    String kolvo_fact = query.getString(4);
                    String kolvo_base = query.getString(5);
                    String employ_full_name = query.getString(6);

                    listDataV.add(i, new String[]{short_namex, user1, user2, timestamp,
                            kolvo_base, kolvo_fact, employ_full_name});
                    i =+1;
                } while (query.moveToNext());
                query.close();
            }
            setInitialData();
        }
    }

};

