package ru.an_sklad.ru;

import static ru.an_sklad.ru.SyncActivity.baseView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.snackbar.Snackbar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import ru.an_sklad.ru.ui.pool.ListStateAdapter;
import ru.an_sklad.ru.ui.pool.State;


public class PoolActivity extends AppCompatActivity {
    ArrayList<String[]> listDataV = new ArrayList<String[]>();
    SQLiteDatabase db;
    ArrayList<State> states = new ArrayList<State>();
    ListView countriesList;
    EditText editTextNumber;
    TextView textViewVName, textViewVCode, textViewProgressBar, textViewArtikul,textViewPlaceFrom,
            textViewUser, textViewKolvo;
    ProgressBar progressBarWait;
    InputMethodManager imm;
    public static boolean listUpdate;
    private ListStateAdapter listStateAdapter;
    private Spinner spinnerHeads;
    String select_head_id;
    List<String> categories = new ArrayList<String>();
    String globalUrl = MainActivity.getStatusUrl();
    Integer countRepeatBar = 1;
    public static AlertDialog.Builder builder;
    ImageButton imageButtonDeleteItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.fragment_pool);

        db = getBaseContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        imm = (InputMethodManager)   getSystemService(Context.INPUT_METHOD_SERVICE);
        getSupportActionBar().hide();

        textViewArtikul = (TextView) findViewById(R.id.textViewArtikul);
        textViewPlaceFrom = (TextView) findViewById(R.id.textViewPlaceFrom);
        textViewUser = (TextView) findViewById(R.id.textViewUser);
        textViewKolvo = (TextView) findViewById(R.id.textViewKolvo);

        textViewVName = (TextView) findViewById(R.id.textViewVName);
        textViewVCode = (TextView) findViewById(R.id.textViewVCode);
        textViewProgressBar = (TextView) findViewById(R.id.textViewProgressBar);
        progressBarWait = (ProgressBar) findViewById(R.id.progressBarWait);
        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        spinnerHeads = (Spinner) findViewById(R.id.spinnerHeads);
        baseView = this.findViewById(R.id.constraintLayout);
        imageButtonDeleteItem = (ImageButton) findViewById(R.id.imageButtonDeleteItem);

        listStateAdapter = new ListStateAdapter(getApplicationContext(), R.layout.list_item_pool, states);
        countriesList = findViewById(R.id.countriesListPool);
        countriesList.setAdapter(listStateAdapter);

        builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);

        spinnerHeads.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    select_head_id = parent.getItemAtPosition(position).toString();
                    select_head_id = select_head_id.split("\\[")[1].split("]")[0];
                    getSqlDataPool();
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getSqlDataHands();

        editTextNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String s = editTextNumber.getText().toString();
                    if(select_head_id.equals("Выберите ревизию")) {
                        Global.ToastMsg("Необходимо выбрать номер ревизии", "error");
                        return false;
                    }else if(!String.valueOf(s.length()).equals("0")) {
                        FilterCommonList(s);
                        editTextNumber.setText("");
                    }else{
                        Global.ToastMsg("Необходимо заполнить поле ШК товара", "error");
                    }
                    return true;
                }
                return false;
            }
        });

        countriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parrent, View itemClicked, int position,
                                    long id) {

                ImageButton imageButtonBarcode = itemClicked.findViewById(R.id.imageButtonBarcode);
                ImageButton imageButtonDeleteItem = itemClicked.findViewById(R.id.imageButtonDeleteItem);
                TextView textViewDeleteItem = itemClicked.findViewById(R.id.textViewDeleteItem);
                TextView textViewKodTov = itemClicked.findViewById(R.id.textViewKodTov);
                TextView textViewScnItem = itemClicked.findViewById(R.id.textViewScnItem);
                TextView textViewPoolId = itemClicked.findViewById(R.id.textViewPoolId);
                imageButtonBarcode.setVisibility(View.VISIBLE);
                textViewScnItem.setVisibility(View.VISIBLE);
                imageButtonDeleteItem.setVisibility(View.VISIBLE);
                textViewDeleteItem.setVisibility(View.VISIBLE);

                String kodTov = textViewKodTov.getText().toString();
                imageButtonBarcode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), ScnActivity.class);
                        intent.putExtra("kodTov", kodTov);
                        startActivity(intent);
                    }
                });

                imageButtonDeleteItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialogSetDelete(position, textViewPoolId);
                    }
                });
            }
        });
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    public static void hideSoftKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.clearFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEtSearch.getWindowToken(), 0);
    }

    public void FilterCommonList(String filter) {
        listUpdate = false;
        hideSoftKeyboard(editTextNumber, getApplicationContext());

        listStateAdapter.getFilter().filter(filter);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {

                } catch (Exception e) {
                }
                if (listUpdate) {
                    //editTextNumber.setText("");
                }else{
                    Global.ToastMsg("Не найдено", "error");
                    editTextNumber.requestFocus();
                    hideSoftKeyboard(editTextNumber, getApplicationContext());
                }

            }
        }, 1000);
    }

    private void getSqlDataPool() {
        progressBarWait.setVisibility(View.VISIBLE);

        String url = globalUrl + "get_pool";

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("head_id", select_head_id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                . writeTimeout(5, TimeUnit.SECONDS)
                .build();

        AndroidNetworking.post(url)
                .addJSONObjectBody(jsonBody)
                .setPriority(Priority.HIGH)
                .setOkHttpClient(okHttpClient)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String sqlDelete = "delete from pool where status = '3'";
                        db.execSQL(sqlDelete);
                        try {
                            JSONArray response_pool = response.getJSONArray("docplace_pool");

                            for (int i = 0; i < Objects.requireNonNull(response_pool).length(); i++) {
                                try {
                                    JSONObject json_data = response_pool.getJSONObject(i);

                                    for (int i2 = 0; i2 < json_data.length(); i2++) {
                                        try {
                                            JSONObject json_data2 = json_data.getJSONObject(String.valueOf(i));
                                            String shelf_id = json_data2.getString("shelf_id");
                                            String shelf_name = json_data2.getString("shelf_name");
                                            String kodtov = json_data2.getString("kodtov");
                                            String kolvo = json_data2.getString("kolvo");
                                            String artikul = json_data2.getString("artikul");
                                            String tov_name = json_data2.getString("tov_name");
                                            String pool_id = json_data2.getString("pool_id");
                                            String comment = json_data2.getString("comment");

                                            String sqlInsert2;

                                            sqlInsert2 = "REPLACE INTO pool (head_id, from_shelf_uid, shelf_name, kodtov, kolvo_fact, artikul, tov_name, pool_id, comment, status ) " +
                                                    "VALUES ('" + select_head_id + "','" + shelf_id + "','" + shelf_name + "', '" + kodtov + "',  '" + kolvo + "',   " +
                                                             "'" + artikul + "', '" + tov_name + "', '" + pool_id + "', '" + comment + "', '3')";

                                            db.execSQL(sqlInsert2);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBarWait.setVisibility(View.GONE);
                        textViewProgressBar.setVisibility(View.GONE);
                        countRepeatBar = 1;
                        getSqlData();
                    }
                    @Override
                    public void onError(ANError error) {
                        textViewProgressBar.setVisibility(View.VISIBLE);
                        countRepeatBar = countRepeatBar + 1;
                        textViewProgressBar.setText(countRepeatBar.toString());
                        textViewProgressBar.setTextColor(Color.RED);

                        getSqlDataPool();
                    }
                });
    }

    private void getSqlDataHands() {
        String head_id=  null;
        String head_name = null;
        String date_of_head = null;

        Cursor query = db.rawQuery("SELECT head_id, head_name, date_of_head FROM  heads", null);
        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    head_id = query.getString(0);
                    head_name = query.getString(1);
                    date_of_head = query.getString(2);

                    categories.add(head_name + " от " + date_of_head + " [" + head_id + "]");
                } while (query.moveToNext());
            }
            query.close();
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHeads.setAdapter(dataAdapter);

        Cursor query2 = db.rawQuery("SELECT head_id FROM common order by _id desc LIMIT 1", null);
        if (query2 != null) {
            if (query2.moveToFirst()) {
                select_head_id = query2.getString(0);
                //getSqlDataPool();
            }
            query2.close();
        }

        Cursor query3 = db.rawQuery("SELECT head_id, head_name, date_of_head FROM heads " +
                "WHERE head_id = '" + select_head_id + "'", null);
        if (query3 != null) {
            if (query3.moveToFirst()) {
                do {
                    head_id = query3.getString(0);
                    head_name = query3.getString(1);
                    date_of_head = query3.getString(2);
                    int spinnerPosition = dataAdapter.getPosition("" + head_name + " от " + date_of_head + " [" + head_id + "]");
                    spinnerHeads.setSelection(spinnerPosition);

                } while (query3.moveToNext());
            }
            query3.close();
        }
    }

    private void getSqlData() {
        if(select_head_id != null) {
            String sql = "" +
                    "SELECT from_shelf_uid, shelf_name, artikul, tov_name, kodtov, kolvo_fact, comment, status, pool_id " +
                    "FROM pool " +
                    "WHERE head_id = '" + select_head_id + "'" +
                    "and status = '3'";

            Cursor query = db.rawQuery(sql, null);
            if (query != null) {
                if (query.moveToFirst()) {
                    int i = 0;
                    do {
                        String from_shelf_id = query.getString(0);
                        String shelf_name = query.getString(1);
                        String artikul = query.getString(2);
                        String tov_name = query.getString(3);
                        String kodtov = query.getString(4);
                        String kolvo_fact = query.getString(5);
                        String comment = query.getString(6);
                        String status = query.getString(7);
                        String poolId = query.getString(8);

                        listDataV.add(i, new String[]{artikul, shelf_name, kolvo_fact, kodtov,
                                comment, tov_name, poolId});
                    } while (query.moveToNext());
                    query.close();
                }
                setListR();

                textViewArtikul.setVisibility(View.VISIBLE);
                textViewPlaceFrom.setVisibility(View.VISIBLE);
                textViewUser.setVisibility(View.VISIBLE);
                textViewKolvo.setVisibility(View.VISIBLE);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void setListR() {
        listStateAdapter.getClear();

        for (String[] strings : listDataV) {
            states.add(new State(strings[0], strings[1], strings[2], strings[3], strings[4], strings[5], strings[6]));
        }
        listDataV.clear();
        listStateAdapter.getAllList();
    }

    public void ClearEditTextNumber(View view) {
        editTextNumber.setText("");
        editTextNumber.requestFocus();
        listStateAdapter.getAllList();
    }

    private void showDialogSetDelete(int position, TextView textViewPoolId) {
        builder.setMessage("Удалить позицию из пула?");

        builder.setPositiveButton("Да", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String url = globalUrl + "set_pool_close";
                String userId = null;
                String poolId = (String) textViewPoolId.getText();

                Cursor query2 = db.rawQuery("SELECT employ_id FROM common order by _id desc LIMIT 1", null);
                if (query2 != null) {
                    if (query2.moveToFirst()) {
                        userId = query2.getString(0);
                    }
                    query2.close();
                }
                JSONObject jsonBody = new JSONObject();
                try {
                    jsonBody.put("user_id", userId);
                    jsonBody.put("pool_id", poolId);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                AndroidNetworking.post(url)
                        .addJSONObjectBody(jsonBody)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.equals("true")) {
                                        String sqlDelete = "delete from pool where  pool_id = '" + poolId + "'";
                                        db.execSQL(sqlDelete);
                                    }
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }

                            }
                            @Override
                            public void onError(ANError error) {
                                Log.e("error", String.valueOf(error));
                            }
                        });
                states.remove(position);
                listStateAdapter.notifyDataSetChanged();
                listStateAdapter.getAllList();

                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Нет", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        builder.show();
    }
};

