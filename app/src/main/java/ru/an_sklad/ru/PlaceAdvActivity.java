package ru.an_sklad.ru;

import static ru.an_sklad.ru.SyncActivity.baseView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.snackbar.Snackbar;
import java.util.ArrayList;
import ru.an_sklad.ru.ui.place_adv.ListStateAdapter;
import ru.an_sklad.ru.ui.place_adv.State;


public class PlaceAdvActivity extends AppCompatActivity {
    ArrayList<String[]> listDataV = new ArrayList<>();
    SQLiteDatabase db;
    ArrayList<State> states = new ArrayList<State>();
    ListView countriesList;
    EditText editTextNumber;
    InputMethodManager imm;
    String recId, shelfUid;
    private ListStateAdapter listStateAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.fragment_place_adv);

        db = getBaseContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        recId = getIntent().getStringExtra("recId");
        shelfUid = getIntent().getStringExtra("shelfUid");

        imm = (InputMethodManager)   getSystemService(Context.INPUT_METHOD_SERVICE);
        getSupportActionBar().hide();
        TextView textViewEmpty = (TextView) findViewById(R.id.textViewEmpty);
        baseView = this.findViewById(R.id.constraintLayout);
        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        editTextNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        String s = editTextNumber.getText().toString();
                        if(!String.valueOf(s.length()).equals("0")) {
                            getDataSQL(recId, s);
                            setListR();
                            textViewEmpty.setVisibility(View.GONE);
                        }else{
                            Global.ToastMsg("Необходимо заполнить поле ШК товара", "error");
                        }
                        editTextNumber.setText("");
                        hideSoftKeyboard(editTextNumber, getApplicationContext());
                    return true;
                }
                return false;
            }
        });
    }

    public static void hideSoftKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.clearFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEtSearch.getWindowToken(), 0);
    }

    private void getDataSQL(String rec_id, String scankod) {

        String sql1 = "" +
                "SELECT kodtov.kodtov, artikul, tov_name " +
                "FROM kodtov " +
                "where kodtov.kodtov = (" +
                    "SELECT kodtov " +
                    "FROM scancodes " +
                    "WHERE scankod = '" + scankod + "'" +
                ")";

        Cursor query1 = db.rawQuery(sql1, null);
        if (query1 != null) {
            if (query1.moveToFirst()) {
                int i = 0;
                do {
                    String kodTov = query1.getString(0);
                    String artikul = query1.getString(1);
                    String tovName = query1.getString(2);

                    listDataV.add(i, new String[]{artikul, "", scankod, rec_id, "", tovName, "", kodTov, ""});
                    i++;
                } while (query1.moveToNext());
                query1.close();
            }
        }
    }

    public void showSoftKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @SuppressLint("SetTextI18n")
    private void setListR() {
        for (String[] strings : listDataV) {
            states.add(new State(strings[0], strings[1], strings[2], strings[3], strings[4], strings[5], strings[6], strings[7], strings[8]));
        }
        listDataV.clear();
        countriesList = findViewById(R.id.countriesListPlaceAdv);
        listStateAdapter = new ListStateAdapter(getApplicationContext(), R.layout.list_item_place_adv, states);
        countriesList.setAdapter(listStateAdapter);
    }

    public void setValueToDBPool(View view) {
        if (listStateAdapter.setValueToDB(shelfUid, countriesList, recId)){
            finish();
        }
    }

    public void ClearEditTextNumber(View view) {
        editTextNumber.setText("");
        editTextNumber.requestFocus();
    }
};
