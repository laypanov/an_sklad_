package ru.an_sklad.ru;

import static ru.an_sklad.ru.SyncActivity.baseView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;
import ru.an_sklad.ru.ui.main2.State;
import ru.an_sklad.ru.ui.main2.ListStateAdapter;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;


public class Main2Activity extends AppCompatActivity {
    public static boolean listUpdate, filterOn;
    public static boolean listUpdateSetClose;
    private static ArrayList<String[][][]> listDataR = new ArrayList<String[][][]>();
    private static String[][][] listDataR2;
    ArrayList<String[]> listDataV = new ArrayList<>();
    ArrayList<State> states = new ArrayList<State>();
    ListView countriesList, constraintLayout3;
    TabLayout tabLayout;
    Button buttonCalc, buttonShowRV, buttonShowRI, buttonSave, buttonClose, buttonPlaceAdv, buttonAddPool, buttonClosePool;
    ImageButton imageButtonBarcode, imageButtonNullItem, imageButtonClearCalc, imageButtonDeleteItemPool;
    EditText editTextCalcResult, editTextNumber, editTextAddPool;
    TextView textViewResult, textViewClearCalc, textViewPolka;
    ViewPager2 pager;
    EditText  editTextCountFact;
    InputMethodManager imm;
    private ListStateAdapter listStateAdapter;
    public static String shortName;
    String recId;
    public static  Integer countDoc;
    private SQLiteDatabase db;
    String shelf_uid, mRecId, mName, place_barcode;
    AlertDialog.Builder  builder;
    int backPressedCount = 0;
    ConstraintLayout ClAddPool;
    int globalPosition;
    View itemClickedGlobal;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = getApplicationContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if(MainActivity.getStatusUserAdmin(db)) {
            setContentView(R.layout.fragment_main2_admin);
        }else{
            setContentView(R.layout.fragment_main2_user);
        }

        mRecId = getIntent().getStringExtra("mRecId");
        mName = getIntent().getStringExtra("mName");
        place_barcode = getIntent().getStringExtra("place_barcode");

        getSupportActionBar().hide();

        buttonCalc = (Button) findViewById(R.id.buttonCalc);
        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonAddPool = (Button) findViewById(R.id.buttonAddPool);
        buttonClosePool = (Button) findViewById(R.id.buttonClosePool);
        buttonClose = (Button) findViewById(R.id.buttonClose);
        buttonShowRV = (Button) findViewById(R.id.buttonShowRV);
        buttonShowRI = (Button) findViewById(R.id.buttonShowRI);
        buttonPlaceAdv = (Button) findViewById(R.id.buttonPlaceAdv);
        imageButtonBarcode = (ImageButton) findViewById(R.id.imageButtonBarcode);
        imageButtonClearCalc = (ImageButton) findViewById(R.id.imageButtonClearCalc);
        imageButtonNullItem = (ImageButton) findViewById(R.id.imageButtonNullItem);
        imageButtonDeleteItemPool = (ImageButton) findViewById(R.id.imageButtonDeleteItemPool);
        textViewResult = (TextView) findViewById(R.id.textViewResult);
        textViewClearCalc = (TextView) findViewById(R.id.textViewClearCalc);
        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        editTextAddPool = (EditText) findViewById(R.id.editTextAddPool);
        textViewPolka = (TextView) findViewById(R.id.textViewPolka);
        editTextCalcResult = (EditText) findViewById(R.id.editTextCalcResult);
        pager = (ViewPager2) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        countriesList = findViewById(R.id.countriesListMain2Common);
        constraintLayout3 = findViewById(R.id.constraintLayout3);
        ClAddPool = findViewById(R.id.ClAddPool);
        baseView = this.findViewById(R.id.constraintLayout);

        countriesList.setScrollingCacheEnabled(false);
        setCalcFunc();
        buttonCalc.performClick();

        editTextNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String s = editTextNumber.getText().toString();
                    if (s.length() >= 3) {
                        String sadv = s.charAt(0) + "" + s.charAt(1) + "" + s.charAt(2);
                        if (!sadv.equals("903")) {
                            editTextNumber.setText("");
                            editTextCalcResult.setText("");
                            FilterCommonList(s);
                        } else {
                            Global.ToastMsg("Необходимо ввести ШК товара", "error");
                            hideSoftKeyboard(editTextNumber, getApplicationContext());
                            ClearEditTextNumber(editTextNumber);
                            editTextNumber.requestFocus();
                        }
                    }
                    return true;
                }
                return false;
            }
        });
        editTextNumber.requestFocus();

        editTextCalcResult.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(itemClickedGlobal != null & filterOn) {
                        String s = textViewResult.getText().toString();
                        listStateAdapter.setValueFact(s.substring(1), 0);
                        editTextCalcResult.setText("");
                        filterOn = false;
                        ClearEditTextNumber(editTextNumber);
                    }
                    hideSoftKeyboard(editTextNumber, getApplicationContext());
                }
                return false;
            }
        });


        editTextAddPool.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String s = editTextAddPool.getText().toString();
                    if(!String.valueOf(s.length()).equals("0")) {
                        editTextAddPool.setText("");
                        if(s.length() > 0) {
                            if (AddGoodsPool(s)) {
                                finish();
                                Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                                intent.putExtra("mRecId", recId);
                                intent.putExtra("mName", mName);
                                intent.putExtra("place_barcode", place_barcode);
                                startActivity(intent);
                            }else{
                                Global.ToastMsg("Нельзя добавить товар который числится на полке", "error");
                            }
                        }else {
                            Global.ToastMsg("Необходимо заполнить поле ШК товара", "error");
                        }
                    }
                    return true;
                }
                return false;
            }
        });
        builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
    }

    private boolean AddGoodsPool(String s) {
        String sql1 = "" +
                "SELECT kodtov.kodtov, artikul, tov_name " +
                "FROM kodtov " +
                "where kodtov.kodtov = (" +
                "SELECT kodtov " +
                "FROM scancodes " +
                "WHERE scankod = '" + s + "'" +
                ")";

        Cursor query1 = db.rawQuery(sql1, null);
        if (query1 != null) {
            if (query1.moveToFirst()) {
                String kodTov = query1.getString(0);
                String artikul = query1.getString(1);
                String tovName = query1.getString(2);

                String sql0 = "select (_id) from docplace_main " +
                        "where " +
                        "shelf_uid = '" + shelf_uid + "' " +
                        "and kodtov = '" + kodTov + "'";

                Cursor query0 = db.rawQuery(sql0, null);
                if (query0 != null) {
                    if (query0.getCount() == 0) {
                        listDataV.clear();
                        listDataV.add(0, new String[]{"3", artikul, "", "1", "1", tovName, "", kodTov, "", recId});
                        listStateAdapter.notifyDataSetChanged();

                        setListR();
                        listStateAdapter.getAllList();

                        String sqlInsert2 = "REPLACE INTO pool (from_docplace_id, to_short_namex, kodtov, " +
                                "kolvo_fact, status, comment, from_shelf_uid) " +
                                "VALUES ('" + recId + "', '', '" + kodTov + "',  '1', '1', '', '" + shelf_uid + "')";

                        db.execSQL(sqlInsert2);
                        query0.close();
                        query1.close();
                        return true;
                    }else{
                        query0.close();
                        query1.close();
                        return false;
                    }
                }
            }
            query1.close();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if(backPressedCount == 5) {
            nextClosePool(editTextNumber);
            backPressedCount = 0;
            return;
        }

        if(backPressedCount == 0){
            Global.ToastMsg("Нажмите повторно для выхода без сохранения", "error");
            backPressedCount++;
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onResume(){
        super.onResume();
        states.clear();
        listDataV.clear();

        getSqlData(mRecId, mName, place_barcode);
        getDataFromSQL();

        Boolean checkList = listStateAdapter.checkValuesList();
        if (checkList){
            buttonSave.setVisibility(View.GONE);
            buttonClose.setVisibility(View.VISIBLE);
        }
        baseView = this.findViewById(R.id.constraintLayout);
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    public static String removeLastCharsPlace(String s) {
        return (s == null || s.length() == 0)
                ? null
                : (s.substring(0, s.length() - 3));
    }

    private void getSqlData(String mRecId, String mName, String place_barcode){
        shortName = removeLastCharsPlace(mName);

        String sqlSelect0 = "" +
                "SELECT rec_id, user1, user2, shelf_uid " +
                "FROM docplace " +
                "where status  = 1 " +
                "and short_namex like '" + shortName + "%' ";

        Cursor query0 = db.rawQuery(sqlSelect0, null);

        if (query0 != null) {
            if (query0.getCount() == 0) {
                String[] employ_ids = new String[2];

                String sql = "SELECT employ_id FROM common";

                Cursor query = db.rawQuery(sql, null);
                if (query != null) {
                    if (query.moveToFirst()) {
                        int i = 0;
                        do {
                            String employ_id = query.getString(0);
                            employ_ids[i] = employ_id;
                            i++;
                        } while (query.moveToNext());
                        query.close();
                    }
                }

                String head_id = null;
                String sql01 = "SELECT head_id " +
                        "FROM heads " +
                        "WHERE selected = 'true'";

                Cursor query01 = db.rawQuery(sql01, null);
                if (query01 != null) {
                    if (query01.moveToFirst()) {
                        do {
                            head_id = query01.getString(0);
                        } while (query01.moveToNext());
                        query01.close();
                    }
                }
                shelf_uid = UUID.randomUUID().toString().toUpperCase(Locale.ROOT);
                String sqlInsert = "INSERT INTO docplace (short_namex, user1, user2, status, place_barcode, head_id, shelf_uid) " +
                        "VALUES ('" + shortName + "', '" + employ_ids[0] + "', '" + employ_ids[1] + "', 1, " +
                        "'" + place_barcode + "', '" + head_id + "', '" + shelf_uid + "')";
                db.execSQL(sqlInsert);

                String sql1 = "SELECT rec_id " +
                        "FROM docplace " +
                        "order by rec_id desc LIMIT 1";

                Cursor query1 = db.rawQuery(sql1, null);
                if (query1 != null) {
                    if (query1.moveToFirst()) {
                        do {
                            recId = query1.getString(0);
                        } while (query1.moveToNext());
                        query1.close();
                    }
                }

                String sqlSelect2 = "" +
                        "SELECT stock.kodtov, kolvo " +
                        "FROM stock " +
                        "left join kodtov on stock.kodtov = kodtov.kodtov " +
                        "where place_name like '" + shortName + "%' ";

                Cursor query2 = db.rawQuery(sqlSelect2, null);
                if (query2 != null) {
                    if (query2.moveToFirst()) {
                        do {
                            String kodtov = query2.getString(0);
                            String kolvo = query2.getString(1);

                            String sqlInsert2 = "INSERT INTO docplace_main (rec_id, kodtov, " +
                                    "comment, kolvo_fact, kolvo_base, shelf_uid) " +
                                    "VALUES ('" + recId + "', '" + kodtov + "', '', '', " +
                                    "'" + kolvo + "', '" + shelf_uid + "')";
                            db.execSQL(sqlInsert2);
                        } while (query2.moveToNext());
                        query2.close();
                    }
                }
                query0.close();
            }else {
                if (query0.moveToFirst()) {
                    String rec_id = query0.getString(0);
                    shelf_uid = query0.getString(3);
                    recId = rec_id;
                }
            }
            query0.close();
        }
        textViewPolka.setText(shortName + "XX");
    }

    private void getDataFromSQL(){
        String sqlSelect0 = "SELECT stock.place_name, stock.kolvo, kodtov.artikul, kodtov.tov_name, docplace_main.comment, docplace_main.kolvo_fact, docplace_main.kodtov, null as ''" +
                "FROM docplace_main, stock, kodtov " +
                "where " +
                "stock.kodtov = docplace_main.kodtov " +
                "and kodtov.kodtov = docplace_main.kodtov " +
                "and kodtov.inhead = 'true' " +
                "and docplace_main.kodtov = stock.kodtov " +
                "and stock.place_name like '" + shortName + "%' " +
                "and docplace_main.rec_id = '" + recId + "'" +
                "UNION " +
                "SELECT null as '', null as '', kodtov.artikul, kodtov.tov_name, pool.comment, pool.kolvo_fact, kodtov.kodtov,  pool.status " +
                "FROM pool, stock, kodtov " +
                "where pool.status  = '1'  and pool.from_docplace_id = '" + recId + "' " +
                "and stock.kodtov = pool.kodtov " +
                "and kodtov.kodtov = pool.kodtov";

        Cursor query0 = db.rawQuery(sqlSelect0, null);
        if (query0.moveToFirst()) {
            int i = 0;
            do {
                String place_name = query0.getString(0);
                String kolvo = query0.getString(1);
                String artikul = query0.getString(2);
                String tov_name = query0.getString(3);
                String comment = query0.getString(4);
                String kolvo_fact = query0.getString(5);
                String kodtov = query0.getString(6);
                String status = query0.getString(7);

                if(status != null){
                    status = "3";

                }else {
                    status = "1";
                }

                listDataV.add(i, new String[]{status, artikul, place_name, kolvo, kolvo_fact, tov_name, "scankod", kodtov, comment, recId});
                i++;
            } while (query0.moveToNext());
            query0.close();
        }
        setListR();
    }


    @SuppressLint("SetTextI18n")
    private void setListR() {
        for (String[] strings : listDataV) {
            states.add(new State(strings[0], strings[1], strings[2], strings[3], strings[4], strings[5], strings[6], strings[7], strings[8], strings[9]));
        }

        countriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parrent, View itemClicked, int position,
                                    long id) {
                ImageButton imageButtonBarcode = itemClicked.findViewById(R.id.imageButtonBarcode);
                TextView textViewKodTov = itemClicked.findViewById(R.id.textViewKodTov);
                TextView textViewScnItem = itemClicked.findViewById(R.id.textViewScnItem);
                TextView textViewNullItem = itemClicked.findViewById(R.id.textViewNullItem);
                ImageButton imageButtonNullItem = itemClicked.findViewById(R.id.imageButtonNullItem);
                imageButtonBarcode.setVisibility(View.VISIBLE);
                textViewScnItem.setVisibility(View.VISIBLE);
                imageButtonNullItem.setVisibility(View.VISIBLE);
                textViewNullItem.setVisibility(View.VISIBLE);

                itemClickedGlobal = itemClicked;
                String kodTov = textViewKodTov.getText().toString();
                imageButtonBarcode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageButtonBarcode.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.border_2));
                        listStateAdapter.setValueToDB(shelf_uid, countriesList, recId, recId, false);

                        Intent intent = new Intent(getApplicationContext(), ScnActivity.class);
                        intent.putExtra("kodTov", kodTov);
                        startActivity(intent);
                    }
                });

                imageButtonNullItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText editTextCountFact = itemClicked.findViewById(R.id.editTextCountFact);
                        TextView countF = itemClicked.findViewById(R.id.textViewCountFact);
                        Button buttonClose = (Button) findViewById(R.id.buttonClose);
                        Button buttonSave = (Button) findViewById(R.id.buttonSave);
                        editTextCalcResult = (EditText) findViewById(R.id.editTextCalcResult);

                        showDialogSet(position, textViewKodTov);

                        Boolean checkList = listStateAdapter.checkValuesList();
                        if (checkList){
                            buttonSave.setVisibility(View.GONE);
                            buttonClose.setVisibility(View.VISIBLE);
                        }
                        editTextNumber.requestFocus();
                        hideSoftKeyboard(editTextNumber, getApplicationContext());
                    }
                });
            }
        });

        if(MainActivity.getStatusUserAdmin(db)) {
            listStateAdapter = new ListStateAdapter(getApplicationContext(), R.layout.list_item_main2_admin_common, states);
        }else{
            listStateAdapter = new ListStateAdapter(getApplicationContext(), R.layout.list_item_main2_user_common, states);
        }
        countriesList.setAdapter(listStateAdapter);

        editTextNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });
    }

    private void showDialogSet(int position, TextView textViewKodTov) {
        builder.setMessage("Выберите для изменения количества");

        builder.setNeutralButton("Задать ноль",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        listStateAdapter.setValueFact("0", position);

                        Boolean checkList = listStateAdapter.checkValuesList();
                        if (checkList){
                            buttonSave.setVisibility(View.GONE);
                            buttonClose.setVisibility(View.VISIBLE);
                        }
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton("Задать вручную", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String kodtov = textViewKodTov.getText().toString();
                String sql = "SELECT scankod " +
                        "FROM scancodes " +
                        "where " +
                        "scancodes.kodtov = '" + kodtov + "' LIMIT 1";

                Cursor query = db.rawQuery(sql, null);
                String barcode = null;

                if (query != null) {
                    if (query.getCount() != 0) {
                        if (query.moveToFirst()) {
                            barcode = query.getString(0);
                        }
                        query.close();
                        FilterCommonList(barcode);
                    }
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void setCalcFunc(){

        editTextCalcResult.addTextChangedListener(new TextWatcher() {
            String lastValueResult;

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // https://stackoverflow.com/a/25131878
                String text = editTextCalcResult.getText().toString();
                String lastchar = "";

                if (text.length() != 0) {
                    lastchar = text.substring(text.length() - 1);

                    int agregate = 0;

                    String operators[] = text.split("[0-9]+");
                    String operands[] = text.split("[*+-]");


                    if (text.length() > 20) {
                        operators = null;
                        operands = null;
                        lastValueResult = "0";
                        textViewResult.setText("=" + 0);
                        editTextCalcResult.setText("0");
                    }

                    try {
                        agregate = Integer.parseInt(operands[0]);
                        for (int i = 1; i < operands.length; i++) {
                            if (operators[i].equals("+"))
                                agregate += Integer.parseInt(operands[i]);
                            else if (operators[i].equals("-"))
                                agregate -= Integer.parseInt(operands[i]);
                            else if (operators[i].equals("*"))
                                agregate *= Integer.parseInt(operands[i]);
                        }
                        lastValueResult = String.valueOf(agregate);
                        textViewResult.setText("=" + lastValueResult);
                        textViewResult.setVisibility(View.VISIBLE);
                    }catch (Exception e) {
                        Global.ToastMsg("Данный символ не используется", "error");
                        try {
                            text = text.replace(lastchar, "");
                            editTextCalcResult.setText(text);
                            editTextCalcResult.setSelection(text.length());
                        }catch (Exception e2) {
                            editTextCalcResult.setText("");
                        }
                    }
                }else{
                    lastValueResult = "0";
                    textViewResult.setText("=" + lastValueResult);
                }
            }
        });
    }

    public void showCalc(View view) {
        imageButtonClearCalc.setVisibility(View.VISIBLE);
        textViewClearCalc.setVisibility(View.VISIBLE);
        buttonCalc.setVisibility(View.GONE);
        editTextCalcResult.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editTextCalcResult, InputMethodManager.SHOW_IMPLICIT);
    }

    public void ShowRI(View view) {
        buttonShowRV.setVisibility(View.VISIBLE);
        countriesList.setVisibility(View.VISIBLE);
        buttonShowRI.setVisibility(View.GONE);
        pager.setVisibility(View.GONE);
        tabLayout.setVisibility(View.GONE);
        editTextNumber.setEnabled(true);
    }

    public static void hideSoftKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.clearFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEtSearch.getWindowToken(), 0);
    }

    public void FilterCommonList(String filter) {
        listUpdate = false;
        filterOn = true;

        listStateAdapter.getFilter().filter(filter);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    View v = countriesList.getChildAt(0);
                    v.requestFocus();
                    itemClickedGlobal = v;
                    editTextCountFact = v.findViewById(R.id.editTextCountFact);
                    editTextCountFact.setOnKeyListener(new View.OnKeyListener() {
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            if ((event.getAction() == KeyEvent.ACTION_UP) &&
                                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                                editTextCountFact = v.findViewById(R.id.editTextCountFact);
                                String editTextFact = editTextCountFact.getText().toString();
                                listStateAdapter.setValueFact(editTextFact, 0);
                                Boolean checkList = listStateAdapter.checkValuesList();
                                if (checkList){
                                    buttonSave.setVisibility(View.GONE);
                                    buttonClose.setVisibility(View.VISIBLE);
                                }
                                editTextNumber.requestFocus();
                                hideSoftKeyboard(editTextNumber, getApplicationContext());
                                return true;
                            }
                            return false;
                        }
                    });
                } catch (Exception e) {
                }
                if (listUpdate) {
                    editTextNumber.setText("");
                    editTextCountFact.setFocusable(true);
                    editTextCountFact.setFocusableInTouchMode(true);
                    editTextCountFact.setEnabled(true);
                    editTextCountFact.requestFocus();

                    editTextCountFact.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0f, 0f, 0));
                    editTextCountFact.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0f, 0f, 0));
                }else{
                    Global.ToastMsg("Не найдено", "error");
                    hideSoftKeyboard(editTextNumber, getApplicationContext());
                    ClearEditTextNumber(editTextNumber);
                }
            }
        }, 500);
    }

    public void setValueToDBSave(View view) {
        if (listStateAdapter.setValueToDB(shelf_uid, countriesList, recId, shortName, false)){
            finish();
        }
    }

    public void setValueToDBClose(View view) {
        if (listStateAdapter.setValueToDB(shelf_uid, countriesList, recId, shortName, true)){
            finish();
        }
    }

    public void ClearEditTextNumber(View view) {
        editTextNumber.setText("");
        editTextNumber.requestFocus();
        listStateAdapter.getAllList();
    }

    public void nextPlaceAdv(View view) {
        listStateAdapter.setValueToDB(shelf_uid, countriesList, recId, shortName, false);

        Intent intent = new Intent(this, PlaceAdvActivity.class);
        intent.putExtra("recId", recId);
        intent.putExtra("shelfUid", shelf_uid);
        startActivity(intent);
    }

    public void editTextCalcResult(View view) {
        editTextCalcResult.setText("");
        editTextCalcResult.requestFocus();
    }

    public void nextAddPool(View view) {
        listStateAdapter.setValueToDB(shelf_uid, countriesList, recId, shortName, false);
        buttonCalc.setVisibility(View.INVISIBLE);
        buttonClose.setVisibility(View.INVISIBLE);
        buttonSave.setVisibility(View.INVISIBLE);
        buttonAddPool.setVisibility(View.INVISIBLE);

        ClAddPool.setVisibility(View.VISIBLE);
        buttonClosePool.setVisibility(View.VISIBLE);
        editTextAddPool.requestFocus();
        backPressedCount = 5;
    }

    public void nextClosePool(View view) {
        buttonCalc.setVisibility(View.VISIBLE);
        buttonAddPool.setVisibility(View.VISIBLE);

        ClAddPool.setVisibility(View.INVISIBLE);
        buttonClosePool.setVisibility(View.INVISIBLE);

        editTextAddPool.setText("");
        editTextAddPool.clearFocus();

        Boolean checkList = listStateAdapter.checkValuesList();
        if (checkList){
            buttonSave.setVisibility(View.GONE);
            buttonClose.setVisibility(View.VISIBLE);
        }else{
            buttonSave.setVisibility(View.VISIBLE);
            buttonClose.setVisibility(View.GONE);
        }
    }

    public void ClearEditTextAddPool(View view) {
        editTextAddPool.setText("");
        editTextAddPool.requestFocus();
    }
};

