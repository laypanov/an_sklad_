package ru.an_sklad.ru;

import static ru.an_sklad.ru.SyncActivity.baseView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import ru.an_sklad.ru.ui.shelfcheck.ListStateAdapter;
import ru.an_sklad.ru.ui.shelfcheck.State;


public class ShelfCheckActivity extends AppCompatActivity {
    public static boolean listUpdate;
    private static ArrayList<String[][][]> listDataR = new ArrayList<String[][][]>();
    private static String[][][] listDataR2;
    ArrayList<String[]> listDataV = new ArrayList<>();
    ArrayList<State> states = new ArrayList<State>();
    ArrayList<String> compliteListDataV = new ArrayList<String>();
    ListView countriesList;
    TabLayout tabLayout;
    Button buttonShowRV, buttonShowRI, buttonSave, buttonClose;
    ImageButton imageButtonBarcode, imageButtonNullItem;
    EditText editTextCalcResult, editTextNumber;
    TextView textViewResult, textViewError, textViewPolka, textViewProgressBar;
    ProgressBar progressBarWait;
    ViewPager2 pager;
    private int lastPosImage;
    EditText  editTextCountFact;
    InputMethodManager imm;
    private ListStateAdapter listStateAdapter;
    public static String shortName;
    String recId;
    public static  Integer countDoc;
    private SQLiteDatabase db;
    Spinner spinnerHeads;
    String select_head_id;
    List<String> categories = new ArrayList<String>();
    String globalUrl = MainActivity.getStatusUrl();
    Integer countRepeatBar = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = getApplicationContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.fragment_shelf_check);

        getSupportActionBar().hide();

        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonClose = (Button) findViewById(R.id.buttonClose);
        buttonShowRV = (Button) findViewById(R.id.buttonShowRV);
        buttonShowRI = (Button) findViewById(R.id.buttonShowRI);
        imageButtonBarcode = (ImageButton) findViewById(R.id.imageButtonBarcode);
        imageButtonNullItem = (ImageButton) findViewById(R.id.imageButtonNullItem);
        textViewResult = (TextView) findViewById(R.id.textViewResult);
        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        textViewPolka = (TextView) findViewById(R.id.textViewPolka);
        editTextCalcResult = (EditText) findViewById(R.id.editTextCalcResult);
        pager = (ViewPager2) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        spinnerHeads = (Spinner) findViewById(R.id.spinnerHeads);
        textViewProgressBar = (TextView) findViewById(R.id.textViewProgressBar);
        progressBarWait = (ProgressBar) findViewById(R.id.progressBarWait);
        baseView = this.findViewById(R.id.constraintLayout);

        spinnerHeads.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    select_head_id = parent.getItemAtPosition(position).toString();
                    select_head_id = select_head_id.split("\\[")[1].split("]")[0];
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerHeads.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    select_head_id = parent.getItemAtPosition(position).toString();
                    select_head_id = select_head_id.split("\\[")[1].split("]")[0];

                    getSqlData();
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        countriesList = findViewById(R.id.countriesListShelfCheck);

        listStateAdapter = new ListStateAdapter(getApplicationContext(), R.layout.list_item_shelf_check, states);

        getSqlDataHands();

        countriesList.setAdapter(listStateAdapter);

        countriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parrent, View itemClicked, int position,
                                    long id) {

                Button buttonDetails = itemClicked.findViewById(R.id.ButtonDetails);
                ImageButton imageButtonBarcode = itemClicked.findViewById(R.id.imageButtonBarcode);
                TextView textViewKodTov = itemClicked.findViewById(R.id.textViewKodTov);
                TextView textViewScnItem = itemClicked.findViewById(R.id.textViewScnItem);
                //TextView textViewPlace1 = itemClicked.findViewById(R.id.textViewPlace1);
                buttonDetails.setVisibility(View.VISIBLE);
                imageButtonBarcode.setVisibility(View.VISIBLE);
                textViewScnItem.setVisibility(View.VISIBLE);

                lastPosImage = position;
                String kodtov = textViewKodTov.getText().toString();

                String kodTov = textViewKodTov.getText().toString();
                imageButtonBarcode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageButtonBarcode.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.border_2));

                        Intent intent = new Intent(getApplicationContext(), ScnActivity.class);
                        intent.putExtra("kodTov", kodTov);
                        startActivity(intent);
                    }
                });

                buttonDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        buttonDetails.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.border_2));
                        Intent intent = new Intent(getApplicationContext(), ShelfCheck2Activity.class);
                        intent.putExtra("kodTov", kodTov);
                        intent.putExtra("head_id", select_head_id);
                        startActivity(intent);
                    }
                });
            }
        });

        editTextNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        String s = editTextNumber.getText().toString();
                        if(!String.valueOf(s.length()).equals("0")) {
                            editTextNumber.setText("");
                            FilterCommonList(s);
                        }else{
                            Global.ToastMsg("Необходимо заполнить поле ШК товара", "error");
                        }
                    return true;
                }
                return false;
            }
        });
    }

    public void ClearEditTextNumber(View view) {
        editTextNumber.setText("");
        editTextNumber.requestFocus();
        listStateAdapter.getAllList();
    }

    public void FilterCommonList(String filter) {
        listUpdate = false;
        hideSoftKeyboard(editTextNumber, getApplicationContext());

        listStateAdapter.getFilter().filter(filter);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {

                } catch (Exception e) {
                }
                if (listUpdate) {
                    //editTextNumber.setText("");
                }else{
                    Global.ToastMsg("Не найдено", "error");
                }

            }
        }, 1000);
    }

    public static void hideSoftKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.clearFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEtSearch.getWindowToken(), 0);
    }

    private void getSqlDataHands() {
        String head_id=  null;
        String head_name = null;
        String date_of_head = null;

        Cursor query = db.rawQuery("SELECT head_id, head_name, date_of_head FROM  heads", null);
        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    head_id = query.getString(0);
                    head_name = query.getString(1);
                    date_of_head = query.getString(2);

                    categories.add(head_name + " от " + date_of_head + " [" + head_id + "]");
                } while (query.moveToNext());
            }
            query.close();
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHeads.setAdapter(dataAdapter);

        Cursor query2 = db.rawQuery("SELECT head_id FROM common order by _id desc LIMIT 1", null);
        if (query2 != null) {
            if (query2.moveToFirst()) {
                select_head_id = query2.getString(0);
            }
            query2.close();
        }

        Cursor query3 = db.rawQuery("SELECT head_id, head_name, date_of_head FROM heads " +
                "WHERE head_id = '" + select_head_id + "'", null);
        if (query3 != null) {
            if (query3.moveToFirst()) {
                do {
                    head_id = query3.getString(0);
                    head_name = query3.getString(1);
                    date_of_head = query3.getString(2);

                    int spinnerPosition = dataAdapter.getPosition("" + head_name + " от " + date_of_head + " [" + head_id + "]");
                    spinnerHeads.setSelection(spinnerPosition);

                } while (query3.moveToNext());
            }
            query3.close();
        }

    }

    private void setInitialData() {
        listStateAdapter.getClear();
        states.clear();

        for (String[] strings : listDataV) {
            states.add(new State(strings[0], strings[1], strings[2], strings[3]));
        }

        listDataV.clear();
        listStateAdapter.getAllList();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }


    @Override
    public void onResume(){
        super.onResume();
        baseView = this.findViewById(R.id.constraintLayout);
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    public static String removeLastCharsPlace(String s) {
        return (s == null || s.length() == 0)
                ? null
                : (s.substring(0, s.length() - 3));
    }

    private void getSqlData(){
        progressBarWait.setVisibility(View.VISIBLE);

        String sqlDelete = "delete from docplace where sync = '1' and status = '4'";
        db.execSQL(sqlDelete);

        String sqlDelete2 = "delete from docplace_main " +
                "where shelf_uid not in (select shelf_uid from docplace)";
        db.execSQL(sqlDelete2);

        String url = globalUrl + "get_shelfs";

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("head_id", select_head_id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        AndroidNetworking.post(url)
                .addJSONObjectBody(jsonBody)
                .setPriority(Priority.HIGH)
                .setOkHttpClient(okHttpClient)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray response_docplace = response.getJSONArray("docplace");

                            for (int i = 0; i < Objects.requireNonNull(response_docplace).length(); i++) {
                                try {
                                    JSONObject json_data = response_docplace.getJSONObject(i);

                                        String shelf_id = json_data.getString("shelf_id");
                                        String shelf_name = json_data.getString("shelf_name");
                                        String user_id1 = json_data.getString("user_id1");
                                        String user_id2 = json_data.getString("user_id2");
                                        String date_of_oper = json_data.getString("date_of_oper");
                                        String place_barcode = null;

                                        Cursor query3 = db.rawQuery("select scan_code from stock " +
                                                "where place_name like '" + shelf_name + "%'  LIMIT 1 ", null);
                                        if (query3 != null) {
                                            if (query3.moveToFirst()) {
                                                place_barcode = query3.getString(0);
                                            }
                                            query3.close();
                                        }

                                        String sqlInsert2;
                                        sqlInsert2 = "REPLACE INTO docplace (head_id, shelf_uid, short_namex, user1, user2, timestamp, sync, status) " +
                                                "VALUES ('" + select_head_id + "','" + shelf_id + "','" + shelf_name + "', '" + user_id1 + "',  '" + user_id2 + "',   " +
                                                "'" + date_of_oper + "', '1', '4')";

                                        db.execSQL(sqlInsert2);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            JSONArray response_main = response.getJSONArray("docplace_main");
                            int response_main_len = Objects.requireNonNull(response_main).length();

                            for (int i = 0; i < response_main_len; i++) {
                                try {
                                    JSONObject json_data = response_main.getJSONObject(i);

                                        String shelf_id = json_data.getString("shelf_id");
                                        String kodtov = json_data.getString("kodtov");
                                        String kolvo_fact = json_data.getString("kolvo_fact");
                                        String kolvo_base = json_data.getString("kolvo_base");
                                        String comment = json_data.getString("comment");

                                        if (i == response_main_len - 1 && !kolvo_base.equals(kolvo_fact)){
                                            String sqlInsert3 = "update docplace set err = 1 where shelf_uid = '" + shelf_id + "'";
                                            db.execSQL(sqlInsert3);
                                        }

                                        String sqlInsert2;
                                        sqlInsert2 = "REPLACE INTO docplace_main (shelf_uid, kodtov, kolvo_fact, kolvo_base, comment) " +
                                                "VALUES ('" + shelf_id + "', '" + kodtov + "', '" + kolvo_fact + "', '" + kolvo_base + "', '" + comment + "')";

                                        db.execSQL(sqlInsert2);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBarWait.setVisibility(View.GONE);
                        textViewProgressBar.setVisibility(View.GONE);
                        countRepeatBar = 1;
                        selectDocsFilterPlace();
                    }
                    @Override
                    public void onError(ANError error) {
                        textViewProgressBar.setVisibility(View.VISIBLE);
                        countRepeatBar = countRepeatBar + 1;
                        textViewProgressBar.setText(countRepeatBar.toString());
                        textViewProgressBar.setTextColor(Color.RED);
                        getSqlData();
                    }
                });
    }

    private void selectDocsFilterPlace(){
        Cursor query = db.rawQuery("select docplace_main.kodtov,  kodtov.artikul, kodtov.tov_name, count(*) as cnt " +
                "from docplace_main, kodtov, docplace " +
                "where docplace_main.kolvo_base != docplace_main.kolvo_fact " +
                "and docplace_main.kodtov = kodtov.kodtov " +
                "and docplace_main.shelf_uid = docplace.shelf_uid " +
                "and docplace.status = 4 and docplace.sync = 1 " +
                "group by docplace_main.kodtov", null);
        if (query != null) {
            if (query.moveToFirst()) {
                int i = 0;

                do {
                    String kodtov = query.getString(0);
                    String artikul = query.getString(1);
                    String tov_name = query.getString(2);
                    String cnt = query.getString(3);
                    listDataV.add(i, new String[]{kodtov, artikul, tov_name, cnt});
                    i =+1;
                } while (query.moveToNext());
                query.close();
            }
            setInitialData();
        }
    }

    private void selectAllPlace(){
        Cursor query = db.rawQuery("SELECT place_id, place_name, scan_code FROM stock ORDER BY place_name DESC", null);
        if (query != null) {
            String[] data = new String[query.getCount()];
            if (query.moveToFirst()) {
                int i = 0;
                do {
                    String place_id = query.getString(0);
                    String place_name = query.getString(1);
                    String scan_code = query.getString(2);
                    i =+1;
                } while (query.moveToNext());
                query.close();
            }
        }
    }

};

