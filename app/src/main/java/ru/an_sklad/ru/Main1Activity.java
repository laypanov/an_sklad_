package ru.an_sklad.ru;

import static ru.an_sklad.ru.SyncActivity.baseView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.material.snackbar.Snackbar;
import java.util.ArrayList;
import ru.an_sklad.ru.ui.main1.State;
import ru.an_sklad.ru.ui.main1.ListStateAdapter;


public class Main1Activity extends AppCompatActivity {
    SQLiteDatabase db;
    ArrayList<State> states = new ArrayList<State>();
    ListView countriesList;
    private ListStateAdapter listStateAdapter;
    private int lastPosImage;
    private String lastBarcode;
    EditText editTextNumber;
    AutoCompleteTextView autoCompleteTextView;
    String autoTCVLast = "";
    public static boolean listUpdate;
    ArrayList<String[]> listDataV = new ArrayList<>();
    ArrayList<String> compliteListDataV = new ArrayList<String>();
    String select_head_id;
    String doFilter = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        db = getApplicationContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);
        getSqlData();

        if (MainActivity.getStatusUserAdmin(db)) {
            setContentView(R.layout.fragment_main1_admin);
        } else {
            setContentView(R.layout.fragment_main1_user);
        }

        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        baseView = this.findViewById(R.id.constraintLayout);

        editTextNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
        editTextNumber.setTypeface(Typeface.DEFAULT);

        if (MainActivity.getStatusUserAdmin(db)) {
            listStateAdapter = new ListStateAdapter(getApplicationContext(), R.layout.list_item_main1_admin, states);
        } else {
            listStateAdapter = new ListStateAdapter(getApplicationContext(), R.layout.list_item_main1_user, states);
        }

        setInitialData();
        countriesList = findViewById(R.id.countriesListMain1Admin);

        countriesList.setAdapter(listStateAdapter);

        countriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parrent, View itemClicked, int position,
                                    long id) {

                Button buttonDetails = itemClicked.findViewById(R.id.ButtonDetails);
                TextView textViewBarcode1 = itemClicked.findViewById(R.id.textViewBarcode1);
                TextView textViewRecId = itemClicked.findViewById(R.id.textViewRecId);
                TextView textViewPlace1 = itemClicked.findViewById(R.id.textViewPlace1);
                buttonDetails.setVisibility(View.VISIBLE);
                lastPosImage = position;
                lastBarcode = textViewBarcode1.getText().toString();
                String recId = textViewRecId.getText().toString();
                String name = textViewPlace1.getText().toString();
                String place_barcode = textViewBarcode1.getText().toString();

                buttonDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        buttonDetails.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.border_2));

                        Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                        intent.putExtra("mRecId", recId);
                        intent.putExtra("mName", name);
                        intent.putExtra("place_barcode", place_barcode);
                        startActivity(intent);
                    }
                });
            }
        });

        final String[] selectionTextViewA = {""};
        AutoCompleteTextView textViewA = (AutoCompleteTextView)
                findViewById(R.id.autoCompleteTextView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, compliteListDataV);
        textViewA.setAdapter(adapter);

        textViewA.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                selectionTextViewA[0] = (String) parent.getItemAtPosition(position);
            }
        });

        autoCompleteTextView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String s = autoCompleteTextView.getText().toString();
                    if (!String.valueOf(s.length()).equals("0")) {
                        FilterCommonList(s);
                        doFilter = s;
                    } else {
                        Global.ToastMsg("Нет данных для поиска", "error");
                    }
                    return true;
                }
                return false;
            }
        });

        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                if (count > 0 && autoTCVLast.equals(s.toString())) {
                    return;
                }
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                int sLength = s.length();
                String str = s.toString();

                if (sLength == 0) {
                    autoCompleteTextView.setInputType(InputType.TYPE_CLASS_TEXT);
                } else {
                    autoCompleteTextView.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
                autoCompleteTextView.setTypeface(Typeface.DEFAULT);

                if (count > 0 & count < before & autoTCVLast.equals(s.toString())) {
                    StringBuilder sbuild = new StringBuilder(str);
                    String str2 = String.valueOf(sbuild.replace(sLength - 1, sLength, ""));
                    autoCompleteTextView.setText(str2);
                    autoTCVLast = str;
                    return;
                } else if (count > 0) {
                    if (sLength > 0 & !Character.isDigit(s.charAt(0))) {
                        if (sLength == 4) {
                            str = s + "-";
                            autoCompleteTextView.setText(str);
                            autoCompleteTextView.setSelection(sLength + 1);
                        } else if (sLength == 7) {
                            str = s + "-";
                            autoCompleteTextView.setText(str);
                            autoCompleteTextView.setSelection(sLength + 1);
                        } else if (sLength == 10) {
                            str = s + "-";
                            autoCompleteTextView.setText(str);
                            autoCompleteTextView.setSelection(sLength + 1);
                        }
                        autoTCVLast = str;
                        autoCompleteTextView.showDropDown();
                    }
                }
            }
        });

        editTextNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String s = editTextNumber.getText().toString();
                    if (s.length() == 13) {
                        String sadv = s.charAt(0) + "" + s.charAt(1) + "" + s.charAt(2);
                        if (sadv.equals("903")) {
                            editTextNumber.setText("");
                            String sqlSelect1 = "" +
                                    "SELECT place_name " +
                                    "FROM places " +
                                    "where scan_code = '" + s + "' ";

                            Cursor query1 = db.rawQuery(sqlSelect1, null);
                            if (query1.getCount() != 0) {
                                if (query1.moveToFirst()) {
                                    String kodtov = query1.getString(0);

                                    Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                                    intent.putExtra("mRecId", "");
                                    intent.putExtra("mName", kodtov);
                                    intent.putExtra("place_barcode", s);

                                    startActivity(intent);
                                }
                            } else {
                                Global.ToastMsg("Место хранения не найдено", "error");
                            }
                            query1.close();
                        } else {
                            Global.ToastMsg("Необходимо вводить ШК места хранения", "error");
                        }
                    } else {
                        Global.ToastMsg("Необходимо заполнить поле ШК места", "error");
                    }
                }
                return false;
            }
        });
        editTextNumber.setText("");
        editTextNumber.requestFocus();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    private void setInitialData() {
        listStateAdapter.getClear();
        states.clear();

        for (String[] strings : listDataV) {
            states.add(new State(strings[0], strings[1], strings[2], strings[3]));
            compliteListDataV.add(strings[0]);
        }
        listStateAdapter.getAllList();
    }

    @Override
    public void onBackPressed() {
        return;
    }

    public void showSoftKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        listStateAdapter.notifyDataSetChanged();
        baseView = this.findViewById(R.id.constraintLayout);
    }

    private void getSqlData(){
        Cursor query = db.rawQuery("select place_id, place_name, scan_code from stock " +
                "where stock.kodtov in (select kodtov from kodtov where inhead = 'true') " +
                "ORDER BY place_name DESC", null);
        if (query != null) {
            String[] data = new String[query.getCount()];
            if (query.moveToFirst()) {
                int i = 0;
                do {
                    String place_id = query.getString(0);
                    String place_name = query.getString(1);
                    String scan_code = query.getString(2);

                    listDataV.add(i, new String[]{place_name, scan_code, place_id, ""});
                    i =+1;
                } while (query.moveToNext());
                query.close();
            }
        }
    }

    public void ClearEditTextNumber(View view) {
        editTextNumber.setText("");
        editTextNumber.requestFocus();
        setInitialData();
    }

    public void ClearAutoCompleteTextView(View view) {
        autoCompleteTextView.setText("");
        autoCompleteTextView.requestFocus();
        listStateAdapter.getAllList();
    }

    public void FilterCommonList(String filter) {
        listUpdate = false;
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        listStateAdapter.getFilter().filter(filter);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (listUpdate) {
                    try {
                        View v = countriesList.getChildAt(0);
                        v.requestFocus();
                        autoCompleteTextView.clearFocus();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    Global.ToastMsg("Нет записей", "error");
                }
            }
        }, 1000);
    }


    public void ShowPool(View view) {
        Intent intent = new Intent(getApplicationContext(), PoolActivity.class);
        startActivity(intent);
    }

    public void GetSearchTov(View view) {
        Intent intent = new Intent(getApplicationContext(), ScnActivity.class);
        startActivity(intent);
    }

    public void ShowRV(View view) {
        Intent intent = new Intent(getApplicationContext(), ShelfCheckActivity.class);
        startActivity(intent);
    }
};

