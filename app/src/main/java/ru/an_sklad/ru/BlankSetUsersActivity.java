package ru.an_sklad.ru;

import static ru.an_sklad.ru.SyncActivity.baseView;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.snackbar.Snackbar;
import java.util.ArrayList;

public class BlankSetUsersActivity extends AppCompatActivity {
    SQLiteDatabase db;

    String employ_id1;
    String user_admin1;
    String[] mydata;
    String pin_code_isp1;

    private EditText PswIsp1;
    private CheckBox checkBoxIsp1;
    private ImageButton imageButtonIsp1;
    private Button buttonNext;
    private AutoCompleteTextView autoCompleteTextViewIsp1;
    private TextView textViewDeleteItemNX, editTextTextName;
    String select_head_id;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_blanksetusers);
        db = getBaseContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        PswIsp1 = (EditText) findViewById(R.id.editTextNumberPasswordIsp1);
        checkBoxIsp1 = (CheckBox) findViewById(R.id.checkBoxIsp1);
        imageButtonIsp1 = (ImageButton) findViewById(R.id.imageButtonIsp1);
        buttonNext = (Button) findViewById(R.id.buttonNext);
        textViewDeleteItemNX = (TextView) findViewById(R.id.textViewDeleteItemNX);
        editTextTextName = (TextView) findViewById(R.id.editTextTextName);
        autoCompleteTextViewIsp1 = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewIsp1);
        PswIsp1.addTextChangedListener(PswIsp1Watcher);
        baseView = this.findViewById(R.id.constraintLayout);


        select_head_id = getIntent().getStringExtra("select_head_id");

        if (autoCompleteTextViewIsp1.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

        ArrayList<String> array = new ArrayList<>();
        Cursor query = db.rawQuery("SELECT employ_id, employ_full_name, job_name  FROM users", null);
        if (query != null) {
            mydata = new String[query.getCount()];
            if (query.moveToFirst()) {
                int i = 0;
                do {
                    mydata[i] = query.getString(1) + " (" + query.getString(2) + ") " + "[" + query.getString(0) + "]";
                    i++;
                } while (query.moveToNext());
                query.close();
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, mydata);

        autoCompleteTextViewIsp1.setAdapter(adapter);

        autoCompleteTextViewIsp1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {

                String employ_id = autoCompleteTextViewIsp1.getEditableText().toString();
                employ_id = employ_id.split("\\[")[1];
                employ_id = employ_id.split("]")[0];

                Cursor query = db.rawQuery("SELECT pin_code, is_admin FROM users WHERE employ_id = '" + employ_id + "'", null);
                if (query != null) {
                    if (query.moveToFirst()) {
                        pin_code_isp1 = query.getString(0);
                        user_admin1 = query.getString(1);
                    }
                    query.close();
                } else {
                    checkBoxIsp1.setVisibility(View.VISIBLE);
                    checkBoxIsp1.isChecked();
                }

                if (user_admin1.equals("True")) {
                    PswIsp1.setVisibility(View.VISIBLE);
                    PswIsp1.requestFocus();

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(PswIsp1, InputMethodManager.SHOW_IMPLICIT);
                }else{
                    checkBoxIsp1.setChecked(true);
                }

                autoCompleteTextViewIsp1.setEnabled(false);
                imageButtonIsp1.setVisibility(View.VISIBLE);
                textViewDeleteItemNX.setVisibility(View.VISIBLE);
                employ_id1 = employ_id;
            }
        });
        GetOpenRev();
    }

    public void GetOpenRev(){
        String docId = "0";
        String docName = "";
        Cursor query2 = db.rawQuery("SELECT docid, docname  FROM ListDocs where status = 1", null);
        if (query2 != null) {
            int counts = query2.getCount();
            if(counts != 0) {
                if (query2.moveToFirst()) {
                    docId = query2.getString(0);
                    docName = query2.getString(1);
                }
            }
            query2.close();
        }

        if (!docId.equals("0")){
            Intent intent = new Intent(this, Blank1Activity.class);
            intent.putExtra("docId", docId);
            intent.putExtra("docName", docName);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private TextWatcher  PswIsp1Watcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {}
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        public void onTextChanged(CharSequence s, int start, int before,int count) {
            if (start > 0) {
                checkPpinIsp1();
            }
        }
    };

    private void checkPpinIsp1(){
        String textIsp1 = PswIsp1.getEditableText().toString();
        if (textIsp1.equals(pin_code_isp1)) {
            checkBoxIsp1.setVisibility(View.VISIBLE);
            imageButtonIsp1.setVisibility(View.VISIBLE);
            checkBoxIsp1.setChecked(true);
            PswIsp1.setEnabled(false);
            autoCompleteTextViewIsp1.setEnabled(false);
            buttonNext.setVisibility(View.VISIBLE);
        }
    };

    public void bnext(View view) {
        if(checkBoxIsp1.isChecked() &&  editTextTextName.getText().length() != 0) {
            String revName = String.valueOf(editTextTextName.getText());
            db.execSQL("INSERT INTO common (employ_id, user_admin) VALUES ('" + employ_id1 + "', '" + user_admin1 + "');");
            db.execSQL("INSERT INTO ListDocs (docname, status, user_id) VALUES ('" + revName + "', '1', '" + employ_id1 + "');");
            GetOpenRev();
        }else{
            Global.ToastMsg("Необходимо заполнить все поля", "error");
        }
    }

    public void ClearUser(View view) {
        checkBoxIsp1.setChecked(false);
        PswIsp1.setEnabled(true);
        autoCompleteTextViewIsp1.setEnabled(true);
        checkBoxIsp1.setVisibility(View.GONE);
        imageButtonIsp1.setVisibility(View.GONE);
        PswIsp1.setVisibility(View.GONE);
        autoCompleteTextViewIsp1.setText("");
        PswIsp1.setText("");
        autoCompleteTextViewIsp1.requestFocus();
    }

}
