package ru.an_sklad.ru;

import org.json.JSONArray;
import org.json.JSONException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class GzToJSON{

    public static void GunZipIt(String inputGzipFile, String outputFile) {
        int BUFFER_SIZE = 8 * 1024;
        byte[] buffer = new byte[BUFFER_SIZE];
        try {
            GZIPInputStream gzis =
            new GZIPInputStream(new FileInputStream(inputGzipFile));
            FileOutputStream out =
            new FileOutputStream(outputFile);
            int len;
            while ((len = gzis.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }

        gzis.close();
        out.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static JSONArray  LoadJSONFromAsset(String file_name) throws JSONException {
        String json = null;
        try {
            InputStream is = new FileInputStream(file_name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        JSONArray jsonArray = new JSONArray(json);
        return jsonArray;
    }
}