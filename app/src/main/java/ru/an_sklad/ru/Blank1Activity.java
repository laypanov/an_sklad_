package ru.an_sklad.ru;

import static ru.an_sklad.ru.SyncActivity.baseView;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import com.google.android.material.snackbar.Snackbar;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import ru.an_sklad.ru.ui.blank1.ListStateAdapterMain;
import ru.an_sklad.ru.ui.blank1.ListStateAdapterLog;
import ru.an_sklad.ru.ui.blank1.StateLog;
import ru.an_sklad.ru.ui.blank1.StateMain;
import ru.an_sklad.ru.ui.main2.State;

public class Blank1Activity extends AppCompatActivity {
    SQLiteDatabase db;
    ArrayList<StateMain> statesMain = new ArrayList<StateMain>();
    ArrayList<StateLog> statesLog = new ArrayList<StateLog>();
    ListView countriesListMain, countriesListLog;
    private ListStateAdapterMain listStateAdapterMain;
    private ListStateAdapterLog listStateAdapterLog;
    EditText editTextCalcResult, editTextNumber;
    TextView textViewRevName, textViewResult, textViewClearCalc;
    public static boolean listUpdate;
    ArrayList<String[]> listDataMain = new ArrayList<>();
    ArrayList<String[]> listDataLog = new ArrayList<>();
    ArrayList<String> compliteListDataMain = new ArrayList<String>();
    ArrayList<String> compliteListDataLog = new ArrayList<String>();
    Button buttonCalc;
    public  static String docId, docName;
    ImageButton imageButtonBarcode, imageButtonClearCalc;
    public static Boolean updateLog = false;
    int backPressedCount = 0;
    AlertDialog.Builder builder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        docId = getIntent().getStringExtra("docId");
        docName = getIntent().getStringExtra("docName");

        db = getApplicationContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        setContentView(R.layout.fragment_blank1);

        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        editTextCalcResult = (EditText) findViewById(R.id.editTextCalcResult);
        textViewRevName = (TextView) findViewById(R.id.textViewRevName);
        imageButtonBarcode = (ImageButton) findViewById(R.id.imageButtonBarcode);
        imageButtonClearCalc = (ImageButton) findViewById(R.id.imageButtonClearCalc);
        buttonCalc = (Button) findViewById(R.id.buttonCalc);
        textViewResult = (TextView) findViewById(R.id.textViewResult);
        textViewClearCalc = (TextView) findViewById(R.id.textViewClearCalc);
        baseView = this.findViewById(R.id.constraintLayout);

        editTextNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
        editTextNumber.setTypeface(Typeface.DEFAULT);

        builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);

        listStateAdapterMain = new ListStateAdapterMain(getApplicationContext(), R.layout.list_item_blank_main, statesMain);
        listStateAdapterLog = new ListStateAdapterLog(getApplicationContext(), R.layout.list_item_blank_log, statesLog);

        setCalcFunc();
        getSqlData();
        setInitialDataMain();
        setInitialDataLog();

        countriesListMain = findViewById(R.id.countriesListMain);
        countriesListMain.setAdapter(listStateAdapterMain);

        countriesListLog = findViewById(R.id.countriesListLog);
        countriesListLog.setAdapter(listStateAdapterLog);

        countriesListMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parrent, View itemClicked, int position,
                                    long id) {

                ImageButton imageButtonBarcode = itemClicked.findViewById(R.id.imageButtonBarcode);
                ImageButton imageButtonDeleteItem = itemClicked.findViewById(R.id.imageButtonDeleteItem);
                ImageButton imageButtonSetVal = itemClicked.findViewById(R.id.imageButtonSetVal);
                TextView textViewScnItem = itemClicked.findViewById(R.id.textViewScnItem);
                TextView textViewDeleteItem = itemClicked.findViewById(R.id.textViewDeleteItem);
                TextView textViewKodTov = itemClicked.findViewById(R.id.textViewKodTov);
                TextView textViewArtikul = itemClicked.findViewById(R.id.textViewArtikul);
                TextView textViewSetVal = itemClicked.findViewById(R.id.textViewSetVal);
                EditText editTextCountFact = itemClicked.findViewById(R.id.editTextCountFact);

                imageButtonBarcode.setVisibility(View.VISIBLE);
                imageButtonDeleteItem.setVisibility(View.VISIBLE);
                textViewScnItem.setVisibility(View.VISIBLE);
                textViewDeleteItem.setVisibility(View.VISIBLE);

                String kodTov = textViewKodTov.getText().toString();

                int pos = editTextCountFact.getText().length();
                editTextCountFact.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.border_1));
                editTextCountFact.setFocusable(true);
                editTextCountFact.setFocusableInTouchMode(true);
                editTextCountFact.setEnabled(true);
                editTextCountFact.requestFocus();
                editTextCountFact.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0f, 0f, 0));
                editTextCountFact.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0f, 0f, 0));
                editTextCountFact.setSelection(pos);


                imageButtonBarcode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageButtonBarcode.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.border_2));
                        //listStateAdapter.setValueToDB(shelf_uid, countriesList, recId, recId, false);

                        Intent intent = new Intent(getApplicationContext(), ScnActivity.class);
                        intent.putExtra("kodTov", kodTov);
                        startActivity(intent);
                    }
                });

                editTextCountFact.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String kodtov = textViewKodTov.getText().toString();
                        String artikul = textViewArtikul.getText().toString();
                        int st = 0;
                        listStateAdapterMain.setValueFact(String.valueOf(s), position);
                        if(s.length() != 0) {
                            st = Integer.parseInt(String.valueOf(s)) - 1;
                        }else{
                            st = -1;
                        }
                        EditMainList(st, kodtov, artikul);
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });

                editTextCountFact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE ||
                                actionId == EditorInfo.IME_ACTION_NEXT) {
                            editTextCountFact.clearFocus();
                            hideSoftKeyboard(editTextCountFact, getApplicationContext());
                            return true;
                        }
                        return false;
                    }
                });
            }
        });

        editTextNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String s = editTextNumber.getText().toString();
                    if(!String.valueOf(s.length()).equals("0")) {
                        editTextNumber.setText("");
                        AddMainList(s);
                    }else{
                        Global.ToastMsg("Необходимо заполнить поле ШК товара", "error");
                    }
                    return true;
                }
                return false;
            }
        });
        editTextNumber.requestFocus();
    }

    @Override
    public void onBackPressed() {
        return;
    }

    private void AddLogList(String action, String s, String artikul, String kodtov) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault());
        String currentDateTime = sdf.format(new Date());

        db.execSQL("INSERT INTO ListOperLog (docid, actions, search, artikul, kodtov, dateofdoc) " +
                "VALUES ('" + docId + "', '" + action + "', '" + s + "', '" + artikul + "', " +
                "'" + kodtov + "', '" + currentDateTime + "');");

        listDataLog.add(0, new String[]{action, s, artikul, kodtov, currentDateTime});
        setInitialDataLog();
    }

    private void EditMainList(int s, String kodtov, String artikul) {
        String sqlUpdate = "update ListOper " +
                "set kolvo = '" + s + "' " +
                "where docid = '" + docId + "'  " +
                "and kodtov = '" + kodtov + "'";
        db.execSQL(sqlUpdate);

        if(s == -1){
            s = 0;
        }else{
            s++;
        }
        AddLogList("Изменение кол-ва", "задано: " + s, artikul, kodtov);
    }

    public static void hideSoftKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.clearFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEtSearch.getWindowToken(), 0);
    }

    private void AddMainList(String s) {
        String artikul = null, kodtov = null, kolvo = null;

        String sql = "SELECT kodtov.artikul, kodtov.kodtov,kodtov.tov_name " +
                "FROM scancodes, kodtov " +
                "where " +
                "scancodes.scankod = '" + s + "' " +
                "and kodtov.kodtov = scancodes.kodtov  ";

        Cursor query = db.rawQuery(sql, null);

        if (query != null) {
            if (query.getCount() != 0) {
                if (query.moveToFirst()) {
                    artikul = query.getString(0);
                    kodtov = query.getString(1);
                    String tov_name = query.getString(2);

                    db.execSQL("INSERT OR REPLACE INTO ListOper (kodtov, docid, kolvo) " +
                            "SELECT " +
                            "'" + kodtov + "' as kodtov, " +
                            "'" + docId + "' as docid, " +
                            "SUM(CASE WHEN kolvo is NULL THEN 1 ELSE kolvo + 1 END)  as kolvo " +
                            "FROM ListOper " +
                            "where docid = '" + docId + "' and kodtov = '" + kodtov + "'");

                    Cursor query0 = db.rawQuery("" +
                            "SELECT " +
                            "SUM(CASE WHEN kolvo is null THEN 1 ELSE kolvo + 1 END)  as kolvo " +
                            "FROM ListOper " +
                            "where docid = '" + docId + "' and kodtov = '" + kodtov + "'", null);
                    if (query0 != null) {
                        int counts = query0.getCount();
                        if(counts != 0) {
                            if (query0.moveToFirst()) {
                                kolvo = query0.getString(0);

                                if(!kolvo.equals("1")) {
                                    int pos = listStateAdapterMain.getDelList(kodtov);
                                    statesMain.remove(pos);
                                }
                            }
                        }
                        query0.close();
                    }
                    statesMain.add(0, new StateMain(artikul, kodtov, tov_name, kolvo));
                    listStateAdapterMain.notifyDataSetChanged();
                }
                query.close();
                editTextNumber.setText("");
                AddLogList("Добавление", s, artikul, kodtov);
            }else{
                AddLogList("Добавление [Ошибка]", s, "", "");
            }
        }
    }

    private void getSqlData() {
        Cursor query1 = db.rawQuery("" +
                "SELECT " +
                "  kodtov, " +
                "  SUM(CASE WHEN kolvo is null THEN 1 ELSE kolvo + 1 END)  as kolvo " +
                "  FROM ListOper " +
                "  where docid = '" + docId + "' " +
                "group by kodtov", null);
        if (query1 != null) {
            int counts = query1.getCount();
            if(counts != 0) {
                String artikul = null, tov_name = null;
                int i = 0;
                if (query1.moveToFirst()) {
                    do {
                        String kodtov = query1.getString(0);
                        String kolvo = query1.getString(1);

                        Cursor query2 = db.rawQuery("select artikul, tov_name from kodtov " +
                                "where kodtov = '" + kodtov + "'", null);
                        if (query2 != null) {
                            int counts2 = query2.getCount();
                            if (counts2 != 0) {
                                if (query2.moveToFirst()) {
                                    artikul = query2.getString(0);
                                    tov_name = query2.getString(1);
                                }
                            }
                            query2.close();
                        }
                        listDataMain.add(i, new String[]{artikul, kodtov, tov_name, kolvo});

                        i++;
                    } while (query1.moveToNext());
                }
            }
            query1.close();
        }

        Cursor query3 = db.rawQuery("SELECT actions, search, artikul, kodtov, dateofdoc " +
                "FROM ListOperLog where docid = '" + docId + "' order by dateofdoc desc", null);
        if (query3 != null) {
            int counts = query3.getCount();
            if(counts != 0) {
                String artikul = null, tov_name = null, actions = null, search = null,
                        kodtov = null, dateofdoc = null;
                int i2 = 0;
                if (query3.moveToFirst()) {
                    do {
                        actions = query3.getString(0);
                        search = query3.getString(1);
                        artikul = query3.getString(2);
                        kodtov = query3.getString(3);
                        dateofdoc = query3.getString(4);

                        listDataLog.add(i2, new String[]{actions, search, artikul, kodtov, dateofdoc});
                        i2++;
                    } while (query3.moveToNext());
                }
            }
            query3.close();
        }
    }

    private void setInitialDataLog() {
        listStateAdapterLog.getClear();
        statesLog.clear();

        for (String[] strings : listDataLog) {
            statesLog.add(new StateLog(strings[0], strings[1], strings[2], strings[3], strings[4]));
            compliteListDataLog.add(strings[0]);
        }
    }

    private void setInitialDataMain() {
        textViewRevName.setText(docName);

        listStateAdapterMain.getClear();
        statesMain.clear();


        for (String[] strings : listDataMain) {
            statesMain.add(new StateMain(strings[0], strings[1], strings[2], strings[3]));
            compliteListDataMain.add(strings[0]);
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    public void ClearEditTextNumber(View view) {
        editTextNumber.setText("");
        editTextNumber.requestFocus();
        //listStateAdapter.getAllList();
    }

    public void editTextCalcResult(View view) {
        editTextCalcResult.setText("");
        editTextCalcResult.requestFocus();
    }

    private void setCalcFunc(){

        editTextCalcResult.addTextChangedListener(new TextWatcher() {
            String lastValueResult;

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // https://stackoverflow.com/a/25131878
                String text = editTextCalcResult.getText().toString();
                String lastchar = "";

                if (text.length() != 0) {
                    lastchar = text.substring(text.length() - 1);

                    int agregate = 0;

                    String operators[] = text.split("[0-9]+");
                    String operands[] = text.split("[*+-]");


                    if (text.length() > 20) {
                        operators = null;
                        operands = null;
                        lastValueResult = "0";
                        textViewResult.setText("=" + 0);
                        editTextCalcResult.setText("0");
                    }

                    try {
                        agregate = Integer.parseInt(operands[0]);
                        for (int i = 1; i < operands.length; i++) {
                            if (operators[i].equals("+"))
                                agregate += Integer.parseInt(operands[i]);
                            else if (operators[i].equals("-"))
                                agregate -= Integer.parseInt(operands[i]);
                            else if (operators[i].equals("*"))
                                agregate *= Integer.parseInt(operands[i]);
                        }
                        lastValueResult = String.valueOf(agregate);
                        textViewResult.setText("=" + lastValueResult);
                        textViewResult.setVisibility(View.VISIBLE);
                    }catch (Exception e) {
                        Global.ToastMsg("Данный символ не используется", "error");
                        text = text.replace(lastchar, "");
                        editTextCalcResult.setText(text);
                        editTextCalcResult.setSelection(text.length());
                    }
                }else{
                    lastValueResult = "0";
                    textViewResult.setText("=" + lastValueResult);
                }
            }
        });
    }

    public void showCalc(View view) {
        imageButtonClearCalc.setVisibility(View.VISIBLE);
        textViewClearCalc.setVisibility(View.VISIBLE);
        buttonCalc.setVisibility(View.GONE);
        editTextCalcResult.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editTextCalcResult, InputMethodManager.SHOW_IMPLICIT);
    }

    public void setValueToDBClose(View view) {
        showDialogSetClose();
    }

    private void showDialogSetClose() {
        builder.setMessage("Документ будет закрыт\nПродолжить работу в нем невозможно");

        builder.setPositiveButton("Да", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                AddLogList("Закрытие документа", "документ ID" + docId, "", "");

                String sqlUpdate = "update ListDocs " +
                        "set status = 2 " +
                        "where " +
                        "docid = '" + docId + "'";
                db.execSQL(sqlUpdate);

                Intent intent = new Intent(getApplicationContext(), BlankSetUsersActivity.class);
                startActivity(intent);
                finish();

                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Нет", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        builder.show();
    }

};

