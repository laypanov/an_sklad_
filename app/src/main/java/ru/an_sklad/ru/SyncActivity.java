package ru.an_sklad.ru;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Pools;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class SyncActivity extends AppCompatActivity {

    Button bnext, btest, bnextBlank;
    SQLiteDatabase db;
    private Context context;
    private Integer download_again = 0;
    private Integer download_done = 0;
    EditText editTextNumber;
    private Spinner spinnerHeads;
    private boolean sync_user = false, sync_stock = false, sync_kodtov = false,
            sync_scancodes = false, sync_heads = false, select_rev = false,
            sync_place_kodtov = false;
    List<String> categories = new ArrayList<String>();
    String select_head_id;
    String globalUrl = MainActivity.getStatusUrl();
    static View baseView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sync);

        db = getBaseContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        bnext = (Button) findViewById(R.id.bnext);
        bnextBlank = (Button) findViewById(R.id.bnextBlank);
        btest = (Button) findViewById(R.id.btest);
        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        spinnerHeads = (Spinner) findViewById(R.id.spinnerHeads);

        createDB();
        getSyncData("heads");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callAsynchronousTask();
            }
        }, 30000);

        spinnerHeads.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    select_head_id = parent.getItemAtPosition(position).toString();
                    select_head_id = select_head_id.split("\\[")[1].split("]")[0];

                    if (position != 0) {
                        bnext.setVisibility(View.INVISIBLE);
                        bnextBlank.setVisibility(View.INVISIBLE);
                        spinnerHeads.setEnabled(false);
                        select_rev = true;
                        sync_user = false;
                        sync_stock = false;
                        sync_scancodes = false;
                        sync_kodtov = false;
                        sync_place_kodtov = false;

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                String sqlSelect2 = "" +
                                        "SELECT timestamp " +
                                        "FROM downloads ";

                                Cursor query2 = db.rawQuery(sqlSelect2, null);
                                if (query2 != null) {
                                    if (query2.moveToFirst()) {
                                        String sqlSelect3 = "" +
                                                "SELECT type_of_rev " +
                                                "FROM heads where head_id = '" + select_head_id + "'";

                                        Cursor query3 = db.rawQuery(sqlSelect3, null);
                                        if (query3 != null) {
                                            if (query3.moveToFirst()) {
                                                int typeR = query3.getInt(0);
                                                if(typeR == 1){
                                                    setDownlodsTime();
                                                    return;
                                                }
                                            }
                                            String timastamp = query2.getString(0);
                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                            try {
                                                Date dateNow = sdf.parse(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
                                                Date date2 = sdf.parse(timastamp);

                                                if (dateNow.after(date2)) {
                                                    setDownlodsTime();
                                                } else {
                                                    bnext.setVisibility(View.VISIBLE);
                                                    bnextBlank.setVisibility(View.VISIBLE);
                                                    spinnerHeads.setEnabled(true);
                                                    return;
                                                }
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                        } else {
                                            setDownlodsTime();
                                        }
                                    }
                                }
                                setDownlodsTime();
                            }

                        }, 1);

                    }
                } catch (Exception e) {
                    bnext.setVisibility(View.INVISIBLE);
                    bnextBlank.setVisibility(View.INVISIBLE);
                    select_rev = false;
                    spinnerHeads.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setDownlodsTime(){
        String sqlDelete = "delete from downloads";
        db.execSQL(sqlDelete);

        getSyncData("kodtov");
        getSyncData("stock");
        getSyncData("places");
        getSyncData("scancodes");
        getSyncData("users");
    }

    public void callAsynchronousTask() {
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            SyncDataTask syncTask = new SyncDataTask();
                            syncTask.execute();
                        } catch (Exception e) {

                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 10000);
    }

    public void setSyncDataFull(View view) {
        String url = globalUrl + "set_full";

        String sqlSelect4 = "" +
                "select docid, dateofdoc, docname, user_id from listdocs " +
                "where status = 2 ";

        Cursor query4 = db.rawQuery(sqlSelect4, null);
        if (query4 != null) {
            if (query4.moveToFirst()) {
                do {
                    JSONObject listDocs = new JSONObject();
                    JSONArray jsonListOperArray = new JSONArray();

                    String docid = query4.getString(0);
                    String dateofdoc = query4.getString(1);
                    String docname = query4.getString(2);
                    String user_id = query4.getString(3);

                    try {
                        listDocs.put("doc_id", docid);
                        listDocs.put("user_id", user_id);
                        listDocs.put("dateofdoc", dateofdoc);
                        listDocs.put("docname", docname);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String sqlSelect5 = "" +
                            "select docid, kodtov, SUM(CASE WHEN kolvo is NULL THEN 1 ELSE kolvo + 1 END)  as kolvo  from listoper " +
                            "where docid = '" + docid + "' ";

                    Cursor query5 = db.rawQuery(sqlSelect5, null);
                    if (query5 != null) {
                        if (query5.moveToFirst()) {
                            do {
                                String kodtov = query5.getString(1);
                                String kolvo = query5.getString(2);

                                JSONObject listOper = new JSONObject();

                                try {
                                    listOper.put("kodtov", kodtov);
                                    listOper.put("kolvo", kolvo);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                jsonListOperArray.put(listOper);

                            } while (query5.moveToNext());
                        }
                    }

                    JSONObject jsonBody = new JSONObject();
                    try {
                        jsonBody.put("list_docs", listDocs);
                        jsonBody.put("list_oper", jsonListOperArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    AndroidNetworking.post(url)
                            .addJSONObjectBody(jsonBody)
                            .setPriority(Priority.HIGH)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    JSONArray response_list_docs = null;
                                    String sqlUpdateDocplace = null;
                                    String doc_id = null;

                                    try {
                                        String result = response.getString("result");
                                        doc_id = response.getString("doc_id");

                                        if (result.equals("true")) {
                                            sqlUpdateDocplace = "UPDATE listDocs " +
                                                    "SET status =  '3' " +
                                                    "WHERE docid = '" + doc_id + "'";
                                        } else {
                                            sqlUpdateDocplace = "UPDATE listDocs " +
                                                    "SET status =  '5' " +
                                                    "WHERE docid = '" + doc_id + "'";
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        sqlUpdateDocplace = "UPDATE listDocs " +
                                                "SET status =  '5' " +
                                                "WHERE docid = '" + doc_id + "'";
                                    }
                                    db.execSQL(sqlUpdateDocplace);
                                }

                                @Override
                                public void onError(ANError error) {
                                    Log.e("error", String.valueOf(error));
                                }
                            });
                } while (query4.moveToNext());
            }
        }
        String sqlSelect1 = "" +
                "SELECT head_id, place_barcode, user1, user2, timestamp, rec_id, shelf_uid " +
                "FROM docplace " +
                "where status = '2'";

        Cursor query1 = db.rawQuery(sqlSelect1, null);
        if (query1 != null) {
            if (query1.moveToFirst()) {
                do {
                    String head_id = query1.getString(0);
                    String place_barcode = query1.getString(1);
                    String user1 = query1.getString(2);
                    String user2 = query1.getString(3);
                    String timestamp = query1.getString(4);
                    String rec_id = query1.getString(5);
                    String shelf_uid = query1.getString(6);

                    if (user2.equals("null")) user2 = "";

                    JSONObject jsonDocplace = new JSONObject();
                    JSONArray jsonDocplaceMainArray = new JSONArray();
                    JSONArray jsonDocplacePoolArray = new JSONArray();
                    try {
                        jsonDocplace.put("head_id", head_id);
                        jsonDocplace.put("place_barcode", place_barcode);
                        jsonDocplace.put("user1", user1);
                        jsonDocplace.put("user2", user2);
                        jsonDocplace.put("timestamp", timestamp);
                        jsonDocplace.put("shelf_uid", shelf_uid);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String sqlSelect2 = "" +
                            "SELECT _id, shelf_uid, kodtov, kolvo_fact, comment " +
                            "FROM docplace_main " +
                            "where good_id is Null and shelf_uid = '" + shelf_uid + "'";

                    Cursor query2 = db.rawQuery(sqlSelect2, null);
                    if (query2 != null) {
                        if (query2.moveToFirst()) {
                            do {
                                String id = query2.getString(0);
                                shelf_uid = query2.getString(1);
                                String kodtov = query2.getString(2);
                                String kolvo_fact = query2.getString(3);
                                String comment = query2.getString(4);

                                JSONObject docplaceMain = new JSONObject();

                                try {
                                    docplaceMain.put("id", id);
                                    docplaceMain.put("shelf_uid", shelf_uid);
                                    docplaceMain.put("kodtov", kodtov);
                                    docplaceMain.put("kolvo", kolvo_fact);
                                    docplaceMain.put("comment", comment);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                jsonDocplaceMainArray.put(docplaceMain);
                            } while (query2.moveToNext());
                        }
                    }

                    String sqlSelect3 = "" +
                            "SELECT _id, from_shelf_uid, kodtov, kolvo_fact, comment " +
                            "FROM pool " +
                            "where sync is Null and from_shelf_uid = '" + shelf_uid + "'";

                    Cursor query3 = db.rawQuery(sqlSelect3, null);
                    if (query3 != null) {
                        if (query3.moveToFirst()) {
                            do {
                                String id = query3.getString(0);
                                String from_shelf_uid = query3.getString(1);
                                String kodtov = query3.getString(2);
                                String kolvo_fact = query3.getString(3);
                                String comment = query3.getString(4);

                                JSONObject docplacePool = new JSONObject();

                                try {
                                    docplacePool.put("id", id);
                                    docplacePool.put("shelf_uid", from_shelf_uid);
                                    docplacePool.put("kodtov", kodtov);
                                    docplacePool.put("kolvo", kolvo_fact);
                                    docplacePool.put("comment", comment);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                jsonDocplacePoolArray.put(docplacePool);

                            } while (query3.moveToNext());
                        }
                    }

                    JSONObject jsonBody = new JSONObject();
                    try {
                        jsonBody.put("docplace", jsonDocplace);
                        jsonBody.put("docplace_main", jsonDocplaceMainArray);
                        jsonBody.put("docplace_pool", jsonDocplacePoolArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String finalShelf_uid = shelf_uid;
                    AndroidNetworking.post(url)
                            .addJSONObjectBody(jsonBody)
                            .setPriority(Priority.HIGH)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    JSONArray response_docplace_main = null;
                                    JSONArray response_docplace_pool = null;
                                    JSONArray response_list_docs = null;
                                    String sqlUpdateDocplace;

                                    try {
                                        JSONObject response_docplace = response.getJSONObject("docplace");
                                        response_docplace_main = response.getJSONArray("docplace_main");
                                        response_docplace_pool = response.getJSONArray("docplace_pool");
                                        response_list_docs = response.getJSONArray("list_docs");

                                        String shelf_uid = response_docplace.getString("shelf_uid");
                                        String result = response_docplace.getString("result");

                                        if(result.equals("true")){
                                            sqlUpdateDocplace = "UPDATE docplace " +
                                                    "SET status =  '3' " +
                                                    "WHERE shelf_uid = '" + shelf_uid + "'";

                                            for (int i = 0; i < Objects.requireNonNull(response_docplace_main).length(); i++) {
                                                try {
                                                    JSONObject json_data = response_docplace_main.getJSONObject(i);

                                                    for (int i2 = 0; i2 < json_data.length(); i2++) {
                                                        try {
                                                            JSONObject json_data2 = json_data.getJSONObject(String.valueOf(i));
                                                            String id = json_data2.getString("id");
                                                            String good_id = json_data2.getString("good_id");

                                                            String sqlUpdateDocplaceMain= "UPDATE docplace_main " +
                                                                    "SET  good_id =  '" + good_id + "'" +
                                                                    "WHERE _id = '" + id + "' ";
                                                            db.execSQL(sqlUpdateDocplaceMain);
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            for (int i = 0; i < Objects.requireNonNull(response_docplace_pool).length(); i++) {
                                                try {
                                                    JSONObject json_data = response_docplace_pool.getJSONObject(i);

                                                    for (int i2 = 0; i2 < json_data.length(); i2++) {
                                                        try {
                                                            JSONObject json_data2 = json_data.getJSONObject(String.valueOf(i));
                                                            String pool_id = json_data2.getString("pool_id");
                                                            String shelf_uid_pool = json_data2.getString("shelf_uid");

                                                            String sqlUpdateDocplacePool = "UPDATE pool " +
                                                                    "SET sync =  'true', " +
                                                                    "pool_id =  '" + pool_id + "' " +
                                                                    "WHERE from_shelf_uid = '" + shelf_uid_pool + "'";
                                                            db.execSQL(sqlUpdateDocplacePool);
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            for (int i = 0; i < Objects.requireNonNull(response_list_docs).length(); i++) {
                                                try {
                                                    JSONObject json_data = response_list_docs.getJSONObject(i);

                                                    for (int i2 = 0; i2 < json_data.length(); i2++) {
                                                        try {
                                                            JSONObject json_data2 = json_data.getJSONObject(String.valueOf(i));
                                                            String pool_id = json_data2.getString("pool_id");
                                                            String shelf_uid_pool = json_data2.getString("shelf_uid");

                                                            String sqlUpdateDocplacePool = "UPDATE pool " +
                                                                    "SET sync =  'true', " +
                                                                    "pool_id =  '" + pool_id + "' " +
                                                                    "WHERE from_shelf_uid = '" + shelf_uid_pool + "'";
                                                            db.execSQL(sqlUpdateDocplacePool);
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }else{
                                            sqlUpdateDocplace = "UPDATE docplace " +
                                                    "SET status =  '5' " +
                                                    "WHERE shelf_uid = '" + shelf_uid + "'";
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        sqlUpdateDocplace = "UPDATE docplace " +
                                                "SET status =  '5' " +
                                                "WHERE shelf_uid = '" + finalShelf_uid + "'";
                                    }
                                    db.execSQL(sqlUpdateDocplace);
                                }

                                @Override
                                public void onError(ANError error) {
                                    Log.e("error", String.valueOf(error));
                                }
                            });
                } while (query1.moveToNext());
                query1.close();
            }
        }
    }

    public void setSyncDataPool(View view) {
        String url = globalUrl + "set_pool";

        String sqlSelect1 = "" +
                "SELECT from_shelf_uid, kodtov, kolvo_fact, comment " +
                "FROM pool " +
                "where sync is Null";

        Cursor query1 = db.rawQuery(sqlSelect1, null);
        if (query1 != null) {
            if (query1.moveToFirst()) {
                do {
                    String from_shelf_uid = query1.getString(0);
                    String kodtov = query1.getString(1);
                    String kolvo_fact = query1.getString(2);
                    String comment = query1.getString(3);

                    AndroidNetworking.post(url)
                            .addBodyParameter("shelf_uid", from_shelf_uid)
                            .addBodyParameter("kodtov", kodtov)
                            .addBodyParameter("kolvo_fact", kolvo_fact)
                            .addBodyParameter("comment", comment)
                            .setPriority(Priority.HIGH)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        String result = response.getString("result");
                                        if (result.equals("true")) {
                                            String shelf_uid = response.getString("shelf_uid");
                                            String pool_id = response.getString("pool_id");

                                            String sqlUpdate = "UPDATE pool " +
                                                    "SET sync =  'true', " +
                                                    "pool_id =  '" + pool_id + "' " +
                                                    "WHERE from_shelf_uid = '" + shelf_uid + "'";
                                            db.execSQL(sqlUpdate);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                @Override
                                public void onError(ANError error) {
                                    Log.e("error", String.valueOf(error));
                                }
                            });
                } while (query1.moveToNext());
                query1.close();
            }
        }
    }

    public void setSyncDataShelf(View view) {
        String url = globalUrl + "set_shelf";

        String sqlSelect1 = "" +
                "SELECT head_id, place_barcode, user1, user2, timestamp, rec_id, shelf_uid " +
                "FROM docplace " +
                "where status = '2'";

        Cursor query1 = db.rawQuery(sqlSelect1, null);
        if (query1 != null) {
            if (query1.moveToFirst()) {
                do {
                    String head_id = query1.getString(0);
                    String place_barcode = query1.getString(1);
                    String user1 = query1.getString(2);
                    String user2 = query1.getString(3);
                    String timestamp = query1.getString(4);
                    String rec_id = query1.getString(5);
                    String shelf_uid = query1.getString(6);

                    if (user2.equals("null")) user2 = "";

                    AndroidNetworking.post(url)
                            .addBodyParameter("Head_ID", head_id)
                            .addBodyParameter("ScanCode", place_barcode)
                            .addBodyParameter("User_ID1", user1)
                            .addBodyParameter("User_ID2", user2)
                            .addBodyParameter("DateOfOper", timestamp)
                            .addBodyParameter("shelf_uid", shelf_uid)
                            .setPriority(Priority.HIGH)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        String result = response.getString("result");
                                        if (result.equals("true")) {
                                            String sqlUpdate = "UPDATE docplace " +
                                                    "SET status =  '3' " +
                                                    "WHERE rec_id = '" + rec_id + "'";
                                            db.execSQL(sqlUpdate);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                @Override
                                public void onError(ANError error) {
                                    Log.e("error", String.valueOf(error));
                                }
                            });
                } while (query1.moveToNext());
                query1.close();
            }
        }
    }

    public void setSyncDataKodTov(View view) {
        String url = globalUrl + "set_kodtov";
        String shelf_uid = "";

        String sqlSelect1 = "" +
                "SELECT shelf_uid " +
                "FROM docplace " +
                "where status = '3'";

        Cursor query1 = db.rawQuery(sqlSelect1, null);
        if (query1 != null) {
            if (query1.moveToFirst()) {
                do {
                    String sqlSelect2 = "" +
                            "SELECT _id, shelf_uid, kodtov, kolvo_fact, comment " +
                            "FROM docplace_main " +
                            "where good_id is Null";

                    Cursor query2 = db.rawQuery(sqlSelect2, null);
                    if (query2 != null) {
                        if (query2.moveToFirst()) {
                            do {
                                String id = query2.getString(0);
                                shelf_uid = query2.getString(1);
                                String kodtov = query2.getString(2);
                                String kolvo_fact = query2.getString(3);
                                String comment = query2.getString(4);

                                String finalShelf_uid = shelf_uid;
                                AndroidNetworking.post(url)
                                        .addBodyParameter("shelf_uid", shelf_uid)
                                        .addBodyParameter("kodtov", kodtov)
                                        .addBodyParameter("kolvo", kolvo_fact)
                                        .addBodyParameter("comment", comment)
                                        .setPriority(Priority.HIGH)
                                        .build()
                                        .getAsJSONObject(new JSONObjectRequestListener() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                try {
                                                    String good_id = response.getString("good_id");
                                                    String kolvo_base = response.getString("kolvo_base");
                                                    String sqlUpdate = "UPDATE docplace_main " +
                                                            "SET  good_id =  '" + good_id + "'" +
                                                            "WHERE _id = '" + id + "' ";
                                                    db.execSQL(sqlUpdate);

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onError(ANError error) {
                                                Log.e("error", String.valueOf(error));
                                            }
                                        });
                            } while (query2.moveToNext());
                        }
                    }
                } while (query1.moveToNext());
            }
        }

        String sqlSelect3 = "" +
                "SELECT shelf_uid " +
                "FROM docplace " +
                "where status = '3'";

        Cursor query3 = db.rawQuery(sqlSelect3, null);
        if (query3 != null) {
            if (query3.moveToFirst()) {
                do {
                    String shelf_uid2 = query3.getString(0);
                    String sqlSelect4 = "" +
                            "SELECT _id " +
                            "FROM docplace_main " +
                            "where shelf_uid = '" + shelf_uid2 + "' and good_id is not Null";
                    Cursor query4 = db.rawQuery(sqlSelect4, null);

                    String sqlSelect5 = "" +
                            "SELECT _id " +
                            "FROM docplace_main " +
                            "where shelf_uid = '" + shelf_uid2 + "'";
                    Cursor query5 = db.rawQuery(sqlSelect5, null);

                    if (query4.getCount() ==  query5.getCount()) {
                        String sqlUpdate2 = "UPDATE docplace " +
                                "SET  status =  '4' " +
                                "WHERE shelf_uid = '" + shelf_uid2 + "'";
                        db.execSQL(sqlUpdate2);
                    }
                } while (query1.moveToNext());
            }
        }
    }

    private void getSyncData(String get_data) {
        String pb_Id  = "pb_" + get_data;
        String percent_Id  = "percent_" + get_data + "_2";
        String b_Id  = "b_" + get_data;

        int pb_IdN = getResources().getIdentifier(pb_Id, "id", this.getPackageName());
        int b_IdN = getResources().getIdentifier(b_Id, "id", this.getPackageName());
        int percent_IdN = getResources().getIdentifier(percent_Id, "id", this.getPackageName());

        TextView percent;
        ProgressBar pb;
        ImageButton ibdownload;

        pb = (ProgressBar) this.findViewById(pb_IdN);
        ibdownload = (ImageButton) this.findViewById(b_IdN);
        percent = (TextView) this.findViewById(percent_IdN);
        pb.setProgress(0);

        try {
            File outputFile = File.createTempFile(
                    "temp_",
                    ".gz"
            );

            String url = globalUrl + "get_" + get_data;

            AndroidNetworking.download(url, outputFile.getParentFile().getPath(), outputFile.getName())
                    .setPriority(Priority.LOW)
                    .addQueryParameter("head_id", select_head_id)
                    .build()

                    .setDownloadProgressListener(new DownloadProgressListener() {
                        private int prevProgress;

                        @Override
                        public void onProgress(final long bytesDownloaded, final long totalBytes) {
                            final int progress = (int) (bytesDownloaded * 100 / totalBytes);

                            if (progress != prevProgress) {
                                pb.setProgressTintList(ColorStateList.valueOf(Color.BLUE));
                                pb.setProgress(progress);
                                percent.setText(progress + " %");
                                prevProgress = progress;

                                Log.e("progress", "total = " + totalBytes +
                                        " loaded = " + bytesDownloaded + " percentage = " + progress + "%");
                            }
                        }
                    })

                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            IntentFilter filter = new IntentFilter();
                            FileToSql(outputFile, get_data);
                            ibdownload.setVisibility(View.INVISIBLE);
                            download_done = download_done + 1;
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.v("progress", String.valueOf(error));
                            pb.setProgressTintList(ColorStateList.valueOf(Color.RED));
                            percent.setText("Ошибка");
                            percent.setTextColor(Color.RED);
                            //dismiss();
                            ibdownload.setVisibility(View.VISIBLE);

                            if (download_again <= 3) {
                                download_again = download_again + 1;
                                getSyncData(get_data);
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void FileDel(File outputFile){
        File fdelete = new File(outputFile.getPath());
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + outputFile.getPath());
            } else {
                System.out.println("file not Deleted :" + outputFile.getPath());
            }
        }

    }

    public void FileToSql(File outputFile, String get_data){

        File outputFile2 = null;
        try {
            outputFile2 = File.createTempFile(
                    "temp_",
                    ".json"
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        GzToJSON.GunZipIt(outputFile.getAbsolutePath(), outputFile2.getAbsolutePath());
        JSONArray jsonArray = null;
        try {
            jsonArray = GzToJSON.LoadJSONFromAsset(outputFile2.getAbsolutePath());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        FileDel(outputFile);
        FileDel(outputFile2);

        //Log.v("json", String.valueOf(jsonArray));

        if (get_data.equals("users")) {
            db.execSQL("DELETE FROM users;");
            String sql = "INSERT OR REPLACE INTO users (" +
                    "employ_id, employ_name, employ_full_name, job_name, is_admin, pin_code) " +
                    "VALUES (?, ?, ?, ?, ?, ?);";

            db.beginTransaction();
            SQLiteStatement statement = db.compileStatement(sql);

            int i;
            for (i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject oneObject = jsonArray.getJSONObject(i);
                    String employ_id = oneObject.getString("employ_id");
                    String employ_name = oneObject.getString("employ_name");
                    String employ_full_name = oneObject.getString("employ_full_name");
                    String job_name = oneObject.getString("job_name");
                    String is_admin = oneObject.getString("is_admin");
                    String pin_code = oneObject.getString("pin_code");

                    statement.bindString(1, employ_id);
                    statement.bindString(2, employ_name);
                    statement.bindString(3, employ_full_name);
                    statement.bindString(4, job_name);
                    statement.bindString(5, is_admin);
                    statement.bindString(6, pin_code);
                    statement.executeInsert();
                    statement.clearBindings();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            sync_user = true;
        }else if (get_data.equals("stock")) {
            db.execSQL("DELETE FROM stock;");
            String sql = "INSERT OR REPLACE INTO stock (" +
                    "place_id, place_name, scan_code, kodtov, kolvo) " +
                    "VALUES (?, ?, ?, ?, ?);";

            db.beginTransaction();
            SQLiteStatement statement = db.compileStatement(sql);

            int i;
            for (i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject oneObject = jsonArray.getJSONObject(i);
                    String place_id = oneObject.getString("place_id");
                    String place_name = oneObject.getString("place_name");
                    String scan_code = oneObject.getString("scan_code");
                    String kodtov = oneObject.getString("kodtov");
                    String kolvo = oneObject.getString("kolvo");

                    statement.bindString(1, place_id);
                    statement.bindString(2, place_name);
                    statement.bindString(3, scan_code);
                    statement.bindString(4, kodtov);
                    statement.bindString(5, kolvo);
                    statement.executeInsert();
                    statement.clearBindings();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            sync_stock = true;
        }else if (get_data.equals("places")) {
            db.execSQL("DELETE FROM places;");
            String sql = "INSERT OR REPLACE INTO places (" +
                    "place_id, place_name, scan_code) " +
                    "VALUES (?, ?, ?);";

            db.beginTransaction();
            SQLiteStatement statement = db.compileStatement(sql);

            int i;
            for (i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject oneObject = jsonArray.getJSONObject(i);
                    String place_id = oneObject.getString("place_id");
                    String place_name = oneObject.getString("place_name");
                    String scan_code = oneObject.getString("scan_code");

                    statement.bindString(1, place_id);
                    statement.bindString(2, place_name);
                    statement.bindString(3, scan_code);
                    statement.executeInsert();
                    statement.clearBindings();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            sync_place_kodtov = true;
        }else if (get_data.equals("kodtov")) {
            db.execSQL("DELETE FROM kodtov;");
            String sql = "INSERT OR REPLACE INTO kodtov (" +
                    "kodtov, artikul, tov_name, inhead) " +
                    "VALUES (?, ?, ?, ?);";

            db.beginTransaction();
            SQLiteStatement statement = db.compileStatement(sql);

            int i;
            for (i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject oneObject = jsonArray.getJSONObject(i);
                    String kodtov = oneObject.getString("kodtov");
                    String artikul = oneObject.getString("artikul");
                    String tov_name = oneObject.getString("tov_name");
                    String inhead = oneObject.getString("inhead");

                    statement.bindString(1, kodtov);
                    statement.bindString(2, artikul);
                    statement.bindString(3, tov_name);
                    statement.bindString(4, inhead);
                    statement.executeInsert();
                    statement.clearBindings();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            sync_kodtov = true;
            spinnerHeads.setEnabled(true);
        }else if (get_data.equals("scancodes")) {
            db.execSQL("DELETE FROM scancodes;");
            String sql = "INSERT OR REPLACE INTO scancodes (" +
                    "kodtov, scankod) " +
                    "VALUES (?, ?);";

            db.beginTransaction();
            SQLiteStatement statement = db.compileStatement(sql);

            int i;
            for (i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject oneObject = jsonArray.getJSONObject(i);
                    String kodtov = oneObject.getString("kodtov");
                    String scankod = oneObject.getString("scankod");

                    statement.bindString(1, kodtov);
                    statement.bindString(2, scankod);
                    statement.executeInsert();
                    statement.clearBindings();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            sync_scancodes = true;
        }else if (get_data.equals("heads")) {
            db.execSQL("DELETE FROM heads;");
            categories.add("Выберите ревизию");

            String sql = "INSERT OR REPLACE INTO heads (" +
                    "head_id, head_name, date_of_head, type_of_rev) " +
                    "VALUES (?, ?, ?, ?);";

            db.beginTransaction();
            SQLiteStatement statement = db.compileStatement(sql);

            int i;
            for (i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject oneObject = jsonArray.getJSONObject(i);
                    String head_id = oneObject.getString("head_id");
                    String head_name = oneObject.getString("head_name");
                    String date_of_head = oneObject.getString("date_of_head");
                    String type_of_rev = oneObject.getString("type_of_rev");

                    categories.add(head_name + " от " + date_of_head + " [" + head_id + "]");

                    statement.bindString(1, head_id);
                    statement.bindString(2, head_name);
                    statement.bindString(3, date_of_head);
                    statement.bindString(4, type_of_rev);
                    statement.executeInsert();
                    statement.clearBindings();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerHeads.setAdapter(dataAdapter);
            sync_heads = true;
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        // temp();
        if (sync_heads){
            spinnerHeads.setVisibility(View.VISIBLE);
        }

        if (sync_user & sync_stock & sync_scancodes & sync_heads & sync_kodtov & sync_place_kodtov){
            bnext.setVisibility(View.VISIBLE);
            bnextBlank.setVisibility(View.VISIBLE);

            String sqlInsert2 = "REPLACE INTO downloads (_id) " +
                    "VALUES ('1')";
            db.execSQL(sqlInsert2);
        }
    }

    public void temp() {
        Cursor query = db.rawQuery("SELECT employ_id, family FROM users", null);
        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    String rec_id = query.getString(1);
                    //Log.v("query2", String.valueOf(rec_id));

                } while (query.moveToNext());
            }
        }
        query.close();
        db.close();
    }

    public void createDB() {
        //db.execSQL("DROP TABLE IF EXISTS users;");
        //db.execSQL("DROP TABLE IF EXISTS stock;");
        //db.execSQL("DROP TABLE IF EXISTS places;");
        //db.execSQL("DROP TABLE IF EXISTS kodtov;");
        //db.execSQL("DROP TABLE IF EXISTS scancodes;");
        //db.execSQL("DROP TABLE IF EXISTS common;");
        //db.execSQL("DROP TABLE IF EXISTS pool;");
        //db.execSQL("DROP TABLE IF EXISTS heads;");

        //db.execSQL("DROP TABLE IF EXISTS docplace;");
        //db.execSQL("DROP TABLE IF EXISTS docplace_main;");
        //db.execSQL("DROP TABLE IF EXISTS pool;");

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS common (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "head_id INT, " +
                        "employ_id INT, " +
                        "user_admin TEXT)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS downloads (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "timestamp DATETIME DEFAULT (strftime('%Y-%m-%d', 'now')))"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS users (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "employ_id INT, " +
                        "employ_name TEXT, " +
                        "employ_full_name TEXT, " +
                        "job_name TEXT," +
                        "is_admin BOOL," +
                        "pin_code INT)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS stock (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "place_id INT, " +
                        "place_name TEXT, " +
                        "scan_code TEXT," +
                        "kodtov TEXT," +
                        "kolvo INT)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS places (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "place_id INT, " +
                        "place_name TEXT, " +
                        "scan_code TEXT)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS kodtov (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "kodtov TEXT, " +
                        "inhead BOOL, " +
                        "artikul TEXT, " +
                        "tov_name TEXT)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS scancodes (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "kodtov TEXT, " +
                        "scankod INT)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS docplace (" +
                        "rec_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "short_namex TEXT, " +
                        "head_id INT, " +
                        "place_id INT, " +
                        "place_barcode INT, " +
                        "shelf_uid TEXT, " +  // от сервера при успешной синх
                        "sync BOOL," +          // синх выполнена
                        "user1 INT, " +
                        "user2 INT, " +
                        "err INT, " +          // косячный док по количеству
                        "status INT, " +  // 1: создан 2: закрыт 3: отправлен 4: полн. закрытие 5: ошибка
                        "timestamp DATETIME DEFAULT (strftime('%Y-%m-%d %H:%M:%f', 'now')))"
        );

        db.execSQL(
                "CREATE UNIQUE INDEX IF NOT EXISTS docplace_idx ON docplace(short_namex, shelf_uid)"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS docplace_main (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "rec_id INT, " +
                        "shelf_uid TEXT, " +
                        "kodtov TEXT, " +
                        "comment TEXT, " +
                        "good_id INT, " +    // от сервера при успешной синх
                        "kolvo_fact INT, " + // количество факт
                        "kolvo_base INT) "   // количество по БД на момент проверки
        );

        db.execSQL(
                "CREATE UNIQUE INDEX IF NOT EXISTS docplace_main_idx ON docplace_main(shelf_uid, kodtov) "
        );

        db.execSQL(
                "CREATE UNIQUE INDEX IF NOT EXISTS docplace_main_idx2 ON docplace_main(rec_id, kodtov) "
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS pool (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "sync BOOL, " +
                        "head_id INT, " +
                        "from_shelf_uid TEXT, " +
                        "from_docplace_id INT, " +   // нах на надо
                        "shelf_name TEXT, " +
                        "artikul TEXT, " +
                        "to_short_namex TEXT, " +
                        "tov_name TEXT, " +
                        "kodtov TEXT, " +
                        "kolvo_fact INT, " +
                        "pool_id INT, " +
                        "status INT, " + // 1: принесли 2: переложили на полку 3: загружен
                        "comment TEXT)" +
                        ""
        );
        db.execSQL(
                "CREATE UNIQUE INDEX IF NOT EXISTS pool_idx ON pool(from_shelf_uid, kodtov) "
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS heads (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "head_id INT, " +
                        "head_name TEXT, " +
                        "type_of_rev INT, " +
                        "date_of_head TEXT, " +
                        "selected BOOL)" +
                        ""
        );
        db.execSQL(
                "CREATE UNIQUE INDEX IF NOT EXISTS heads_idx ON heads(head_id) "
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS ListDocs (" +
                        "docid INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "dateofdoc DATETIME DEFAULT (strftime('%Y-%m-%d %H:%M:%f', 'now')), " +
                        "status INT, " +
                        "user_id INT, " +
                        "docname TEXT)" +
                        ""
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS ListOper (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "kodtov TEXT, " +
                        "docid INT, " +
                        "status INT, " +
                        "kolvo INT)" +
                        ""
        );

        db.execSQL(
                "CREATE UNIQUE INDEX IF NOT EXISTS listoper_idx ON ListOper(kodtov, docid) "
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS ListOperLog (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "docid INT, " +
                        "kodtov TEXT, " +
                        "actions TEXT, " +
                        "artikul TEXT, " +
                        "search TEXT, " +
                        "dateofdoc DATETIME DEFAULT (strftime('%Y-%m-%d %H:%M:%f', 'now'))) " +
                        ""
        );
    };


    public void GetUsers(View view){
        getSyncData("users");
    };

    public void GetStock(View view){
        getSyncData("stock");
    };

    public void GetPlaces(View view){
        getSyncData("places");
    };

    public void GetKodTov(View view){
        getSyncData("kodtov");
    };

    public void GetScaneCodes(View view){
        getSyncData("scancodes");
    };

    public void GetScaneHeads(View view){
        getSyncData("heads");
    };

    public void bnext(View view) {
        String sqlUpdate = "UPDATE heads " +
                "SET  selected =  'true' " +
                "WHERE head_id = '" + select_head_id + "'";
        db.execSQL(sqlUpdate);

        Intent intent = new Intent(this, SetUsers.class);
        intent.putExtra("select_head_id", select_head_id);
        startActivity(intent);
    }

    public void bnextBlank(View view) {
        Intent intent = new Intent(this, BlankSetUsersActivity.class);
        startActivity(intent);
    }


    class SyncDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            setSyncDataFull(btest);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

}
