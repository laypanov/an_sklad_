package ru.an_sklad.ru;

import static ru.an_sklad.ru.SyncActivity.baseView;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

public class Global {
    public static void ToastMsg(String msg, String status) {
        Snackbar snackbar = Snackbar.make(baseView, msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();

        if(status.equals("error")) {
            sbView.setBackgroundColor(Color.GRAY);
            snackbar.setTextColor(Color.WHITE);
        }
        if(status.equals("done")) {
            sbView.setBackgroundColor(Color.WHITE);
            snackbar.setTextColor(Color.BLACK);
        }
        snackbar.show();
    }
}
