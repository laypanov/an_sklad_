package ru.an_sklad.ru.ui.blank1;

import static android.content.Context.MODE_PRIVATE;
import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;
import java.util.List;
import ru.an_sklad.ru.Blank1Activity;
import ru.an_sklad.ru.R;

public class ListStateAdapterLog extends ArrayAdapter<StateLog> implements Filterable {

    private LayoutInflater inflater;
    private int layout;
    private List<StateLog> stateLogs;
    private List<StateLog> itemsModelListFiltered;

    public ListStateAdapterLog(Context context, int resource, List<StateLog> stateLogs) {
        super(context, resource, stateLogs);
        this.stateLogs = stateLogs;
        this.itemsModelListFiltered = stateLogs;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itemsModelListFiltered.size();
    }

    @Override
    public StateLog getItem(int position) {
        return itemsModelListFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SQLiteDatabase db = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        View view=this.inflater.inflate(this.layout, parent, false);

        TextView action = view.findViewById(R.id.textViewAction);
        TextView search = view.findViewById(R.id.textViewSearch);
        TextView artikul = view.findViewById(R.id.textViewArtikul);
        TextView kodtov = view.findViewById(R.id.textViewKodTov);
        TextView date = view.findViewById(R.id.textViewDate);

        StateLog stateLog = itemsModelListFiltered.get(position);

        action.setText(stateLog.getAction());
        search.setText(stateLog.getSearch());
        artikul.setText(stateLog.getArtikul());
        kodtov.setText(stateLog.getKodtov());
        date.setText(stateLog.getDate());

        if(action.getText().equals("Добавление [Ошибка]") ||
                action.getText().equals("Удаление")){
            view.setBackgroundColor(Color.RED);
        }

        return view;
    }

    public void getClear() {
        stateLogs.clear();
        notifyDataSetChanged();
    }

    public void getAllList() {
        itemsModelListFiltered = stateLogs;
        notifyDataSetChanged();
    }
}
