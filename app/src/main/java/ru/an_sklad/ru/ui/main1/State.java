package ru.an_sklad.ru.ui.main1;

public class State {

    private String place; // место хранения
    private String barcode1; // ШК МХ
    private String recId; // rec_id
    private String countR1; // количество расхождений

    public State(String textViewPlace, String textViewBarcode1, String textViewRecId, String textViewCountR1){
        this.place=textViewPlace;
        this.barcode1=textViewBarcode1;
        this.recId=textViewRecId;
        this.countR1=textViewCountR1;
    }

    public String getPlace() {
        return this.place;
    }

    public String getRecId() {
        return this.recId;
    }

    public String getBarcode1() {
        return this.barcode1;
    }

    public String getcountR1() {
        return this.countR1;
    }

    public void setPlace(String place) {
        this.place = place;
    }
    public void getBarcode1(String barcode1) { this.barcode1 = barcode1; }
    public void getRecId(String recId) { this.recId = recId; }
    public void getcountR1(String countR1) { this.countR1 = countR1; }
}