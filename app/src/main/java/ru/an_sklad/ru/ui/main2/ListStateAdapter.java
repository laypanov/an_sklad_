package ru.an_sklad.ru.ui.main2;

import static android.content.Context.MODE_PRIVATE;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import java.util.ArrayList;
import java.util.List;
import ru.an_sklad.ru.Global;
import ru.an_sklad.ru.Main2Activity;
import ru.an_sklad.ru.PlaceAdvActivity;
import ru.an_sklad.ru.R;

public class ListStateAdapter extends  ArrayAdapter<State> implements Filterable, CompoundButton.OnCheckedChangeListener {

    private LayoutInflater inflater;
    private int layout;
    private List<State> states;
    private List<State> itemsModelListFiltered;
    private int position;
    private View convertView;
    private ViewGroup parent;

    public ListStateAdapter(Context context, int resource, List<State> states) {
        super(context, resource, states);
        this.states = states;
        this.itemsModelListFiltered = states;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itemsModelListFiltered.size();
    }

    @Override
    public State getItem(int position) {
        return itemsModelListFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=this.inflater.inflate(this.layout, parent, false);

        TextView recId = view.findViewById(R.id.textViewRecId);
        TextView status = view.findViewById(R.id.textViewStatus);
        TextView vCode = view.findViewById(R.id.textViewVCode);
        TextView place = view.findViewById(R.id.textViewPlace);
        TextView countB = view.findViewById(R.id.textViewCountBase);
        TextView countF = view.findViewById(R.id.textViewCountFact);
        EditText countFEdit = view.findViewById(R.id.editTextCountFact);
        TextView vName = view.findViewById(R.id.textViewVName);
        TextView kodTov = view.findViewById(R.id.textViewKodTov);
        ImageButton imageButtonDeleteItem = view.findViewById(R.id.imageButtonDeleteItem);
        ImageButton imageButtonNullItem = view.findViewById(R.id.imageButtonNullItem);
        TextView textViewDeleteItem = view.findViewById(R.id.textViewDeleteItem);
        Switch switch1 = view.findViewById(R.id.switchToPool);
        EditText comment = view.findViewById(R.id.editTextTextComment);

        State state = itemsModelListFiltered.get(position);

        recId.setText(state.getRecId());
        status.setText(state.getStatus());
        vCode.setText(state.getVCode());
        place.setText(state.getVPlace());
        //barcode.setText("[" + state.getBarcode() + "]");
        countB.setText(state.getCountBase());
        countF.setText(state.getCountFact());
        countFEdit.setText(state.getCountFact());
        vName.setText(state.getVName());
        kodTov.setText(state.getKodTov());
        comment.setText(state.getComment());

        String st = String.valueOf(status.getText());

        if(st.equals("1")){
            imageButtonDeleteItem.setVisibility(View.INVISIBLE);
            textViewDeleteItem.setVisibility(View.INVISIBLE);
            switch1.setVisibility(View.INVISIBLE);
            comment.setVisibility(View.INVISIBLE);
        }else{
            view.setBackgroundColor(Color.LTGRAY);
        }

        if(st.equals("3")){
            switch1.setChecked(true);
            place.setVisibility(View.INVISIBLE);
            countB.setVisibility(View.INVISIBLE);
            countFEdit.setEnabled(true);
            countFEdit.setFocusable(true);
            countFEdit.setFocusableInTouchMode(true);
            comment.setHint("Комментарий");
        }

        if(st.equals("2")){
            switch1.setChecked(false);
        }

        if (switch1 != null) {
            switch1.setOnCheckedChangeListener(this);
        }

        if(countF.getText().length() != 0) {
            if (!countF.getText().equals(countB.getText())) {
                countB.setTextColor(Color.RED);
            }
        }

        imageButtonNullItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValueFact("0", position);
            }
        });

        imageButtonDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db  = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);
                TextView v1 = view.findViewById(R.id.textViewKodTov);
                TextView v2 = view.findViewById(R.id.textViewRecId);
                String sKodTov = String.valueOf(v1.getText());
                String recId = String.valueOf(v2.getText());

                String sqlDelete = "delete from pool where  from_docplace_id = '" + recId + "' and  kodtov = '" + sKodTov + "'";
                db.execSQL(sqlDelete);
                try {
                    states.remove(position);
                } catch (Exception e) {
                    getAllList();
                }
                notifyDataSetChanged();
                getAllList();
            }
        });

        countFEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        countFEdit.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0f, 0f, 0));
                    return true;
                }
                return false;
            }
        });

        countFEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                int pos = countFEdit.getText().length();
                if (hasFocus) {
                    if(countFEdit.getText().toString().equals("0")){
                        countFEdit.setText("");
                    }
                    countFEdit.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.border_1));
                    if(countFEdit.length() > 0) {
                        countFEdit.setSelection(pos);
                    }
                }
            }
        });

        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                State state = itemsModelListFiltered.get(position);
                state.setComment(String.valueOf(s));
                itemsModelListFiltered = states;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        countFEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (st.equals("3")) {
                    SQLiteDatabase db = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);
                    String recIdV = recId.getText().toString();
                    String kodTovV = kodTov.getText().toString();
                    String kolvoV = s.toString();
                    String commentV = comment.getText().toString();
                    countF.setText(s);

                    String sql = "select shelf_uid from docplace where rec_id = '" + recIdV + "' ";

                    Cursor query = db.rawQuery(sql, null);
                    if (query != null) {
                        if (query.moveToFirst()) {
                            String shelf_uid = query.getString(0);
                            String sqlInsert2 = "REPLACE INTO pool (from_shelf_uid, from_docplace_id, kodtov, " +
                                    "kolvo_fact, status, comment) " +
                                    "VALUES ('" + shelf_uid + "','" + recIdV + "', '" + kodTovV + "',  '" + kolvoV + "', '1', '" + commentV + "')";
                            db.execSQL(sqlInsert2);
                        }
                        query.close();
                        db.close();
                    }
                }
            }

            @Override
            public void beforeTextChanged (CharSequence s,int start, int count, int after){
            }

            @Override
            public void afterTextChanged (Editable s){
            }
        });

        return view;
    }
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<State> resultsModel = new ArrayList<>();

                if(constraint == null || constraint.length() == 0){
                    filterResults.count = states.size();
                    filterResults.values = states;
                    Main2Activity.listUpdate = false;
                }else{
                    for(State itemsModel: states){
                        if (getSqlScn(itemsModel.getKodTov(), constraint.toString()) ||
                                constraint.equals(itemsModel.getKodTov())){
                            Main2Activity.listUpdate = true;
                            resultsModel.add(itemsModel);
                        }
                        filterResults.count = resultsModel.size();
                        filterResults.values = resultsModel;
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemsModelListFiltered = (List<State>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    public boolean getSqlScn(String getKodTov, String constraint ) {
        SQLiteDatabase db  = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);
        String sql = "" +
                "SELECT scankod " +
                "FROM scancodes " +
                "where kodtov = '" + getKodTov + "' ";

        Cursor query = db.rawQuery(sql, null);
        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    String scankod = query.getString(0);
                    if(constraint.equals(scankod)){
                        return true;
                    }
                } while (query.moveToNext());
                query.close();
                db.close();
            }
        }
        return false;
    }

    public void setValueFact(String s, int position) {
        State state = itemsModelListFiltered.get(position);
        state.setCountFactEdit(s);
        state.setCountFact(s);
        itemsModelListFiltered = states;
        notifyDataSetChanged();
    }

    public boolean setValueToDB(String shelf_uid, ListView listStateAdapter, String recId, String shortName, Boolean close) {

        SQLiteDatabase db  = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);
        itemsModelListFiltered = states;
        notifyDataSetChanged();

        int i = 0;
        for(State itemsModel: states){
            if (!recId.equals("") && !(itemsModel.getCountFact()).equals("")){
                View v = listStateAdapter.getChildAt(i);
                Switch switch1 = v.findViewById(R.id.switchToPool);

                String comment = itemsModel.getComment();
                String kolvo = itemsModel.getCountFact();
                String kodTov = itemsModel.getKodTov();
                String countB = itemsModel.getCountBase();
                String status = itemsModel.getStatus();
                String sqlInsert2;
                if (status.equals("3")) {
                    sqlInsert2 = "REPLACE INTO pool (from_shelf_uid, from_docplace_id, kodtov, " +
                            "kolvo_fact, status, comment) " +
                            "VALUES ('" + shelf_uid + "','" + recId + "', '" + kodTov + "',  '" + kolvo + "', '1', '" + comment + "')";
                }else {
                    sqlInsert2 = "REPLACE INTO docplace_main (rec_id, kodtov, " +
                            "comment, kolvo_fact, kolvo_base, shelf_uid) " +
                            "VALUES ('" + recId + "', '" + kodTov + "', " +
                            " '" + comment + "', '" + kolvo + "', '" + countB + "', '" + shelf_uid + "')";
                }
                db.execSQL(sqlInsert2);

            }else{
            }
            i =+ 1;
        }

        if(close) {
            String sqlUpdate = "UPDATE docplace " +
                    "SET status = '2'" +
                    "WHERE  rec_id = '" + recId + "'";
            db.execSQL(sqlUpdate);
        }

        Global.ToastMsg("Сохранено", "done");
        return true;
    }

    public void getAllList() {
        itemsModelListFiltered = states;
        notifyDataSetChanged();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    public Boolean checkValuesList() {
        for(State itemsModel: states) {
            if ((itemsModel.getCountFact()).equals("")) {
                return false;
            }
        }
        return true;
    }
}
