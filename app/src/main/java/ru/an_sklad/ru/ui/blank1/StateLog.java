package ru.an_sklad.ru.ui.blank1;

public class StateLog {

    private String action;      // действие
    private String search;     // строка поиска
    private String artikul;     // артикул
    private String kodtov;      // код товара
    private String date;        // дата

    public StateLog(String action, String search, String artikul, String kodtov, String date){

        this.action=action;
        this.search=search;
        this.artikul=artikul;
        this.kodtov=kodtov;
        this.date=date;
    }

    public String getAction() {
        return this.action;
    }
    public String getSearch() {
        return this.search;
    }
    public String getArtikul() {
        return this.artikul;
    }
    public String getKodtov() {
        return this.kodtov;
    }
    public String getDate() {
        return this.date;
    }

    public void setAction(String action) {this.action = action;}
    public void setSearch(String search) {this.search = search;}
    public void setArtikul(String artikul) {this.artikul = artikul;}
    public void setKodtov(String kodtov) {this.action = kodtov;}
    public void setDate(String date) {this.date = date;}
}