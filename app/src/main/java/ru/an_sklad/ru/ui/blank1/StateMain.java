package ru.an_sklad.ru.ui.blank1;

public class StateMain {

    private String artikul; // артикул
    private String kodtov;  // код товара
    private String tovName;  // название товара
    private String kolvo;  // название товара

    public StateMain(String artikul, String kodtov, String tovName, String kolvo){

        this.artikul=artikul;
        this.kodtov=kodtov;
        this.tovName = tovName;
        this.kolvo = kolvo;
    }

    public String getArtikul() {
        return this.artikul;
    }
    public String getKodtov() {
        return this.kodtov;
    }
    public String getTovName() {
        return this.tovName;
    }
    public String getKolvo() {
        return this.kolvo;
    }

    public void setArtikul(String artikul) {this.artikul = artikul;}
    public void setKodtov(String kodtov) {this.kodtov = kodtov;}
    public void setTovName(String tovName) {this.tovName = tovName;}
    public void setKolvo(String kolvo) {this.kolvo = kolvo;}
}