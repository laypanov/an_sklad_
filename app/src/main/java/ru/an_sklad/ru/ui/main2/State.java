package ru.an_sklad.ru.ui.main2;

import android.graphics.Color;

public class State {

    private String status; // статус 1: обычный; 2: остался на полке; 3: отправлен в пул
    private String vCode; // артикул
    private String place;  // место хранения
    private String countB; // количество по БД
    private String countF; // количество по факту
    private String countFEdit; // количество по факту
    private String vName; // название позиции
    private String barcode; // ШК
    private String comment; // комментарий
    private String kodTov; // код товара
    private String recId; // код товара

    public State(String textViewStatus, String textViewVCode, String textViewPlace,
                 String textViewCountBase, String textViewCountFact,
                 String textViewVName, String textViewBarcode, String textViewKodTov, String textViewComment, String textViewRecId){

        this.recId=textViewRecId;
        this.status=textViewStatus;
        this.vCode=textViewVCode;
        this.place=textViewPlace;
        this.barcode=textViewBarcode;
        this.countB=textViewCountBase;
        this.countF=textViewCountFact;
        this.countFEdit=textViewCountFact;
        this.vName=textViewVName;
        this.comment=textViewComment;
        this.kodTov=textViewKodTov;
    }

    public String getRecId() {
        return this.recId;
    }
    public String getStatus() {
        return this.status;
    }
    public String getVCode() {
        return this.vCode;
    }
    public String getVPlace() {
        return this.place;
    }
    public String getBarcode() {
        return this.barcode;
    }
    public String getCountBase() {
        return this.countB;
    }
    public String getCountFact() {
        return this.countF;
    }
    public String getCountEditFact() {
        return this.countFEdit;
    }
    public String getVName() {
        return this.vName;
    }
    public String getComment() {
        return this.comment;
    }
    public String getKodTov() {return this.kodTov;}


    public void setRecId(String recId) {this.recId = recId;}
    public void setStatus(String status) {this.status = status;}
    public void setVCode(String vCode) {
        this.vCode = vCode;
    }
    public void setPlace(String place) { this.place = place; }
    public void setBarcode(String barcode) { this.barcode = barcode; }
    public void setCountBase(String countBase) { this.countB = countBase; }
    public void setCountFact(String countFact) {
        this.countF = countFact;
    }
    public void setCountFactEdit(String countFEdit) {
        this.countFEdit = countFEdit;
    }
    public void setVName(String vName) { this.vName = vName; }
    public void setComment(String comment) { this.comment = comment; }
    public void setKodTov(String kodTov) { this.kodTov = kodTov; }
}