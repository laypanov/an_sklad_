package ru.an_sklad.ru.ui.pool;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import ru.an_sklad.ru.MainActivity;
import ru.an_sklad.ru.PoolActivity;
import ru.an_sklad.ru.R;


public class ListStateAdapter extends  ArrayAdapter<State> implements Filterable {

    private LayoutInflater inflater;
    private int layout;
    private List<State> states;
    private List<State> itemsModelListFiltered;
    String globalUrl = MainActivity.getStatusUrl();

    public ListStateAdapter(Context context, int resource, List<State> states) {
        super(context, resource, states);
        this.states = states;
        this.itemsModelListFiltered = states;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        try {
            return itemsModelListFiltered.size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public State getItem(int position) {
        return itemsModelListFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=this.inflater.inflate(this.layout, parent, false);

        TextView art = view.findViewById(R.id.textViewArt);
        TextView shelfName = view.findViewById(R.id.textViewShelfName);
        TextView kolvo = view.findViewById(R.id.textViewKolvo);
        TextView kodTov = view.findViewById(R.id.textViewKodTov);
        TextView comment = view.findViewById(R.id.editTextComment);
        TextView status = view.findViewById(R.id.textViewStatus);
        TextView vName = view.findViewById(R.id.textViewVName);
        TextView poolId = view.findViewById(R.id.textViewPoolId);
        ImageButton imageButtonDeleteItem = view.findViewById(R.id.imageButtonDeleteItem);

        State state = itemsModelListFiltered.get(position);

        art.setText(state.getArt());
        shelfName.setText(state.getShelfName());
        kolvo.setText(state.getKolvo());
        kodTov.setText(state.getKodTov());
        comment.setText(state.getComment());
        vName.setText(state.getVName());
        poolId.setText(state.getPoolId());

        return view;
    }

    public void getAllList() {
        itemsModelListFiltered = states;
        notifyDataSetChanged();
    }

    public void getClear() {
        states.clear();
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<State> resultsModel = new ArrayList<>();

                if(constraint == null || constraint.length() == 0){
                    filterResults.count = states.size();
                    filterResults.values = states;
                    PoolActivity.listUpdate = false;
                }else{
                    for(State itemsModel: states){
                        if (getSqlScn(itemsModel.getKodTov(), constraint.toString()) ||
                                constraint.equals(itemsModel.getKodTov())){
                            PoolActivity.listUpdate = true;
                            resultsModel.add(itemsModel);
                        }
                        filterResults.count = resultsModel.size();
                        filterResults.values = resultsModel;
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemsModelListFiltered = (List<State>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    public boolean getSqlScn(String getKodTov, String constraint ) {
        SQLiteDatabase db = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);
        String sql = "" +
                "SELECT scankod " +
                "FROM scancodes " +
                "where kodtov = '" + getKodTov + "' ";

        Cursor query = db.rawQuery(sql, null);
        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    String scankod = query.getString(0);
                    if(constraint.equals(scankod)){
                        return true;
                    }
                } while (query.moveToNext());
                query.close();
                db.close();
            }
        }
        return false;
    }
}
