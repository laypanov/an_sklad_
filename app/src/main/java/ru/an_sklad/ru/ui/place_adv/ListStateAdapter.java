package ru.an_sklad.ru.ui.place_adv;

import static android.content.Context.MODE_PRIVATE;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import java.util.List;
import ru.an_sklad.ru.Global;
import ru.an_sklad.ru.PlaceAdvActivity;
import ru.an_sklad.ru.R;

public class ListStateAdapter extends ArrayAdapter<State> implements Filterable {

    private LayoutInflater inflater;
    private int layout;
    private List<State> states;
    private List<State> itemsModelListFiltered;
    public Context context1;

    public ListStateAdapter(Context context, int resource, List<State> states) {
        super(context, resource, states);
        this.states = states;
        this.itemsModelListFiltered = states;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itemsModelListFiltered.size();
    }

    @Override
    public State getItem(int position) {
        return itemsModelListFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=this.inflater.inflate(this.layout, parent, false);

        TextView vCode = view.findViewById(R.id.textViewVCode);
        TextView place = view.findViewById(R.id.textViewPlace);
        TextView barcode1 = view.findViewById(R.id.textViewBarcode);
        TextView recId = view.findViewById(R.id.textViewRecId);
        EditText countF = view.findViewById(R.id.editTextCountFact);
        TextView vName = view.findViewById(R.id.textViewVName);
        TextView kodTov = view.findViewById(R.id.textViewKodTov);
        EditText comment = view.findViewById(R.id.editTextTextComment);
        ImageButton imageButtonDeleteItem = view.findViewById(R.id.imageButtonDeleteItem);

        State state = itemsModelListFiltered.get(position);

        vCode.setText(state.getvCode());
        barcode1.setText(state.getBarcode1());
        recId.setText(state.getRecId());
        countF.setText(state.getcountF());
        vName.setText(state.getVName());
        comment.setText(state.getcomment());
        kodTov.setText(state.getKodTov());

        imageButtonDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                states.remove(position);
                notifyDataSetChanged();
            }
        });

        countF.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                state.setcountF(s.toString());
                countF.setBackgroundColor(Color.WHITE);
            }
        });

        comment.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                state.setcomment(s.toString());
            }
        });

        return view;
    }

    public boolean setValueToDB(String shelfUid, ListView listStateAdapter, String recId) {
        SQLiteDatabase db = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        int i = 0;
        for(State itemsModel: states){
            if (!itemsModel.getRecId().equals("") && !itemsModel.getcountF().equals("")) {
                View v = listStateAdapter.getChildAt(i);
                Switch switch1 = v.findViewById(R.id.switchToPool);
                String status = "2";
                String comment = itemsModel.getcomment();
                String kolvo = itemsModel.getcountF();
                String kodTov = itemsModel.getKodTov();
                String countB = itemsModel.getcountB();

                if(switch1.isChecked()) status = "3";
                String sqlInsert2 = "REPLACE INTO pool (from_docplace_id, to_short_namex, kodtov, " +
                        "kolvo_fact, status, comment, from_shelf_uid) " +
                        "VALUES ('" + recId + "', '', '" + kodTov + "',  '" + kolvo + "', '1', '" + comment + "', '" + shelfUid + "')";
                db.execSQL(sqlInsert2);

            }else{
                View v = listStateAdapter.getChildAt(i);
                View countF = v.findViewById(R.id.editTextCountFact);
                countF.setBackgroundColor(Color.RED);
                Global.ToastMsg("Необходимо заполнить количество", "error");
                return false;
            }
            i =+ 1;
        }
        Global.ToastMsg("Сохранено", "done");
        return true;
    }
}
