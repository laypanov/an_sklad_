package ru.an_sklad.ru.ui.shelfcheck2;

import static android.content.Context.MODE_PRIVATE;
import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;
import java.util.List;

import ru.an_sklad.ru.R;

public class ListStateAdapter extends ArrayAdapter<State> implements Filterable {

    private LayoutInflater inflater;
    private int layout;
    private List<State> states;
    private List<State> itemsModelListFiltered;

    public ListStateAdapter(Context context, int resource, List<State> states) {
        super(context, resource, states);
        this.states = states;
        this.itemsModelListFiltered = states;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itemsModelListFiltered.size();
    }

    @Override
    public State getItem(int position) {
        return itemsModelListFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SQLiteDatabase db = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        View view=this.inflater.inflate(this.layout, parent, false);

        TextView shortNamex = view.findViewById(R.id.textViewShelfName);
        TextView timestamp = view.findViewById(R.id.textViewDateData);
        TextView kolvoBase = view.findViewById(R.id.TextViewCountBaseData);
        TextView kolvoFact = view.findViewById(R.id.TextViewCountFactData);
        TextView employFullName = view.findViewById(R.id.textViewUsersData);
        TextView countNowData = view.findViewById(R.id.TextViewCountNowData);

        State state = itemsModelListFiltered.get(position);

        shortNamex.setText(state.getShortNamex());
        timestamp.setText(state.getTimestamp());
        kolvoBase.setText(state.getKolvoBase());
        kolvoFact.setText(state.getkolvoFact());
        employFullName.setText(state.getEmployFullName());

        if (!kolvoBase.getText().toString().equals(kolvoFact.getText().toString())){
            kolvoFact.setTextColor(Color.RED);
        }
        return view;
    }

    public void getClear() {
        states.clear();
        notifyDataSetChanged();
    }

    public void getAllList() {
        itemsModelListFiltered = states;
        notifyDataSetChanged();
    }
}
