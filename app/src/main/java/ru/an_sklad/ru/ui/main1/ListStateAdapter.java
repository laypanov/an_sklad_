package ru.an_sklad.ru.ui.main1;

import static android.content.Context.MODE_PRIVATE;
import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import java.util.ArrayList;
import java.util.List;
import ru.an_sklad.ru.Main1Activity;
import ru.an_sklad.ru.R;

public class ListStateAdapter extends ArrayAdapter<State> implements Filterable {

    private LayoutInflater inflater;
    private int layout;
    private List<State> states;
    private List<State> itemsModelListFiltered;
    private List<State> itemsModelListFiltered2;

    public ListStateAdapter(Context context, int resource, List<State> states) {
        super(context, resource, states);
        this.states = states;
        this.itemsModelListFiltered = states;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itemsModelListFiltered.size();
    }

    @Override
    public State getItem(int position) {
        return itemsModelListFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SQLiteDatabase db = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        View view = this.inflater.inflate(this.layout, parent, false);

        TextView place = view.findViewById(R.id.textViewPlace1);
        TextView barcode1 = view.findViewById(R.id.textViewBarcode1);
        TextView textViewRecId = view.findViewById(R.id.textViewRecId);
        TextView recId = view.findViewById(R.id.textViewRecId);
        TextView countR1 = view.findViewById(R.id.textViewCountR1);
        Button ButtonDetails = view.findViewById(R.id.ButtonDetails);

        State state = itemsModelListFiltered.get(position);


        place.setText(state.getPlace());
        barcode1.setText(state.getBarcode1());
        recId.setText(state.getRecId());
        countR1.setText(state.getcountR1());

        String full_name = String.valueOf(place.getText());

        Cursor query = db.rawQuery("" +
                "select status from docplace " +
                "where substr('" + full_name + "', -3, -20)  =  docplace.short_namex " +
                "order by status desc", null);

        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    int status = query.getInt(0);
                    if (status > 1){
                        place.setTextColor(Color.BLACK);
                        barcode1.setTextColor(Color.BLACK);
                        textViewRecId.setTextColor(Color.BLACK);
                        if(itemsModelListFiltered2 != null) {
                            itemsModelListFiltered = itemsModelListFiltered2;
                            notifyDataSetChanged();
                        }else{
                            itemsModelListFiltered = states;
                            notifyDataSetChanged();
                        }
                    }
                } while (query.moveToNext());
            }
        }
        query.close();

        return view;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<State> resultsModel = new ArrayList<>();

                constraint = constraint.toString().toLowerCase();

                if(constraint == null || constraint.length() == 0){
                    filterResults.count = states.size();
                    filterResults.values = states;
                    Main1Activity.listUpdate = false;
                }else{
                    for(State itemsModel: states){
                        String itemsPlace = itemsModel.getPlace().toLowerCase();
                        String itemsBarcode = itemsModel.getBarcode1().toLowerCase();
                        if(itemsPlace.contains(constraint.toString()) ||
                            itemsBarcode.contains(constraint.toString())){
                            Main1Activity.listUpdate = true;
                            resultsModel.add(itemsModel);
                        }
                        filterResults.count = resultsModel.size();
                        filterResults.values = resultsModel;
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemsModelListFiltered = (List<State>) results.values;
                itemsModelListFiltered2 = (List<State>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    public void getAllList() {
        itemsModelListFiltered = states;
        itemsModelListFiltered2 = states;
        notifyDataSetChanged();
    }

    public void getClear() {
        states.clear();
        notifyDataSetChanged();
    }

}
