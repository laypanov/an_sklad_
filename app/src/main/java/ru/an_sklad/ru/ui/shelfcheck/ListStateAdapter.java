package ru.an_sklad.ru.ui.shelfcheck;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import java.util.ArrayList;
import java.util.List;
import ru.an_sklad.ru.Main1Activity;
import ru.an_sklad.ru.PoolActivity;
import ru.an_sklad.ru.R;
import ru.an_sklad.ru.ShelfCheckActivity;

public class ListStateAdapter extends ArrayAdapter<State> implements Filterable {

    private LayoutInflater inflater;
    private int layout;
    private List<State> states;
    private List<State> itemsModelListFiltered;

    public ListStateAdapter(Context context, int resource, List<State> states) {
        super(context, resource, states);
        this.states = states;
        this.itemsModelListFiltered = states;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itemsModelListFiltered.size();
    }

    @Override
    public State getItem(int position) {
        return itemsModelListFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SQLiteDatabase db = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        View view=this.inflater.inflate(this.layout, parent, false);

        TextView kodtov = view.findViewById(R.id.textViewKodTov);
        TextView artikul = view.findViewById(R.id.textViewArtikul);
        TextView tovName = view.findViewById(R.id.textViewTovName);
        TextView count = view.findViewById(R.id.textViewCount);
        TextView count2 = view.findViewById(R.id.textViewCount2);

        State state = itemsModelListFiltered.get(position);

        kodtov.setText(state.getKodTov());
        artikul.setText(state.getArtikul());
        tovName.setText(state.getTovName());
        count.setText(state.getCount());

        String kodtovFinal = kodtov.getText().toString();

        Cursor query = db.rawQuery("select count(1) " +
                "from docplace_main " +
                "where " +
                "docplace_main.kodtov = '" + kodtovFinal + "' " +
                "group by docplace_main.kodtov ", null);

        if (query != null) {
            if (query.moveToFirst()) {
                String countSql = query.getString(0);
                count2.setText(countSql);
                notifyDataSetChanged();
            }
        }
        query.close();

        return view;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<State> resultsModel = new ArrayList<>();

                if(constraint == null || constraint.length() == 0){
                    filterResults.count = states.size();
                    filterResults.values = states;
                    ShelfCheckActivity.listUpdate = false;
                }else{
                    for(State itemsModel: states){
                        if (getSqlScn(itemsModel.getKodTov(), constraint.toString()) ||
                                constraint.equals(itemsModel.getKodTov())){
                            ShelfCheckActivity.listUpdate = true;
                            resultsModel.add(itemsModel);
                        }
                        filterResults.count = resultsModel.size();
                        filterResults.values = resultsModel;
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemsModelListFiltered = (List<State>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }


    public boolean getSqlScn(String getKodTov, String constraint ) {
        SQLiteDatabase db = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);
        String sql = "" +
                "SELECT scankod " +
                "FROM scancodes " +
                "where kodtov = '" + getKodTov + "' ";

        Cursor query = db.rawQuery(sql, null);
        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    String scankod = query.getString(0);
                    if(constraint.equals(scankod)){
                        return true;
                    }
                } while (query.moveToNext());
                query.close();
                db.close();
            }
        }
        return false;
    }

    public void getClear() {
        states.clear();
        notifyDataSetChanged();
    }

    public void getAllList() {
        itemsModelListFiltered = states;
        notifyDataSetChanged();
    }
}
