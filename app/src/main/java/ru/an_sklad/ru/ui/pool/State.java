package ru.an_sklad.ru.ui.pool;

import android.widget.TextView;

import ru.an_sklad.ru.R;

public class State {
    private String art;             //  артикул
    private String shelfName;       //  название полки
    private String kolvo;           //  количество
    private String kodTov;          //  Код товара
    private String comment;         //  комментарий
    private String vName;           //  название товара
    private String status;          //  название
    private String poolId;          //  pool_id

    public State(String textViewArt, String textViewShelfName, String textViewKolvo, String textViewKodTov,
                 String editTextComment, String textViewVName, String textViewPoolId){

        this.art=textViewArt;
        this.shelfName=textViewShelfName;
        this.kolvo=textViewKolvo;
        this.kodTov=textViewKodTov;
        this.comment=editTextComment;
        this.vName=textViewVName;
        this.poolId=textViewPoolId;
    }

    public String getArt() {return this.art;}
    public String getShelfName() {return this.shelfName;}
    public String getKolvo() {return this.kolvo;}
    public String getKodTov() {return this.kodTov;}
    public String getComment() {return this.comment;}
    public String getVName() {return this.vName;}
    public String getPoolId() {return this.poolId;}

    public void setArt(String art) { this.art = art; }
    public void setShelfName(String shelfName) { this.shelfName = shelfName; }
    public void setKolvo(String kolvo) { this.kolvo = kolvo; }
    public void setComment(String comment) { this.comment = comment; }
    public void setKodtov(String kodTov) { this.kodTov = kodTov; }
    public void setVName(String vName) { this.vName = vName; }
    public void setPoolId(String poolId) { this.poolId = poolId; }
}
