package ru.an_sklad.ru.ui.shelfcheck;

public class State {

    private String kodtov; // код товара
    private String artikul; // артикул
    private String tovName; // Названгие товара
    private String scankod; // ШК
    private String count; // количество расхождений


    public State(String textViewKodtov, String textViewArtikul,
                 String textViewTovName, String textViewCount){
        this.kodtov=textViewKodtov;
        this.artikul=textViewArtikul;
        this.tovName=textViewTovName;
        this.count=textViewCount;
    }

    public String getKodTov() {
        return this.kodtov;
    }

    public String getArtikul() {
        return this.artikul;
    }

    public String getTovName() {
        return this.tovName;
    }

    public String getCount() { return this.count; }

    public void setKodtov(String kodtov) {
        this.kodtov = kodtov;
    }
    public void setArtikul(String artikul) { this.artikul = artikul; }
    public void setTovName(String tovName) { this.tovName = tovName; }
    public void setCount(String count) { this.count = count; }
}