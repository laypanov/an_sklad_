package ru.an_sklad.ru.ui.scn;

public class State {
    private String barcode; // ШК
    private String kodTov; // Код товара
    private String scankod; // ШК по которому искали
    private String placeName; // название места хранения
    private String kolvo; // количество
    private String nameTov; // количество
    private String zonaId; // зона
    private String mesto_id; // зона

    public State(String textViewBarcode, String textViewScanKod, String textViewKodTov, String textViewPlaceName, String textViewKolvo,
                 String textViewNameTov, String textViewZonaVal, String textViewMestoId){
        this.barcode=textViewBarcode;
        this.scankod=textViewScanKod;

        this.kodTov=textViewKodTov;
        this.placeName=textViewPlaceName;
        this.kodTov=textViewKodTov;
        this.kolvo=textViewKolvo;
        this.nameTov=textViewNameTov;
        this.zonaId=textViewZonaVal;
        this.mesto_id=textViewMestoId;
    }

    public String getBarcode() {
        return this.barcode;
    }
    public String getScankod() {
        return this.scankod;
    }
    public String getKodTov() {return this.kodTov;}
    public String getPlaceName() {return this.placeName;}
    public String getKolvo() {return this.kolvo;}
    public String getNameTov() {return this.nameTov;}
    public String getZonaId() {return this.zonaId;}
    public String getMestoId() {return this.mesto_id;}

    public void setBarcode(String barcode) { this.barcode = barcode; }
    public void setScankod(String scankod) { this.scankod = scankod; }
    public void setKodTov(String kodTov) { this.kodTov = kodTov; }
    public void setPlaceName(String placeName) { this.placeName = placeName; }
    public void setKolvo(String kolvo) { this.kolvo = kolvo; }
    public void setNameTov(String nameTov) { this.nameTov = nameTov; }
    public void setZonaId(String zonaId) { this.zonaId = zonaId; }
    public void setMestoId(String mesto_id) { this.mesto_id = mesto_id; }
}


