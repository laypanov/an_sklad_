package ru.an_sklad.ru.ui.blank1;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import ru.an_sklad.ru.Blank1Activity;
import ru.an_sklad.ru.R;
import ru.an_sklad.ru.ui.main2.State;


public class ListStateAdapterMain extends ArrayAdapter<StateMain> implements Filterable {

    private LayoutInflater inflater;
    private int layout;
    private List<StateMain> stateMains;
    private List<StateMain> itemsModelListFiltered;
    private ListStateAdapterLog listStateAdapterLog;
    public ListStateAdapterMain(Context context, int resource, List<StateMain> stateMains) {
        super(context, resource, stateMains);
        this.stateMains = stateMains;
        this.itemsModelListFiltered = stateMains;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itemsModelListFiltered.size();
    }

    @Override
    public StateMain getItem(int position) {
        return itemsModelListFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SQLiteDatabase db = getContext().openOrCreateDatabase("an_sklad.db", MODE_PRIVATE, null);

        View view=this.inflater.inflate(this.layout, parent, false);

        TextView artikul = view.findViewById(R.id.textViewArtikul);
        TextView kodtov = view.findViewById(R.id.textViewKodTov);
        EditText kolvo = view.findViewById(R.id.editTextCountFact);
        TextView tovName = view.findViewById(R.id.textViewVName);
        StateMain stateMain = itemsModelListFiltered.get(position);
        artikul.setText(stateMain.getArtikul());
        kodtov.setText(stateMain.getKodtov());
        kolvo.setText(stateMain.getKolvo());
        tovName.setText(stateMain.getTovName());

        ImageButton imageButtonDeleteItem = view.findViewById(R.id.imageButtonDeleteItem);

        imageButtonDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView v1 = view.findViewById(R.id.textViewKodTov);
                String kodtov = String.valueOf(v1.getText());
                String docid = Blank1Activity.docId;

                String sqlDelete = "delete from ListOper where docid = '" + docid + "' and  kodtov = '" + kodtov + "'";
                db.execSQL(sqlDelete);

                stateMains.remove(position);
                notifyDataSetChanged();
                getAllList();

                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault());
                String currentDateTime = sdf.format(new Date());

                String art = artikul.getText().toString();

                String sqlInsert = "INSERT INTO ListOperLog (docid, actions, search, artikul, kodtov, dateofdoc) " +
                        "VALUES ('" + docid + "', 'Удаление', '', '" + art + "', " +
                        "'" + kodtov + "', '" + currentDateTime + "')";
                db.execSQL(sqlInsert);
                Blank1Activity.updateLog = true;
            }
        });
        return view;
    }

    public void getClear() {
        stateMains.clear();
        notifyDataSetChanged();
    }

    public void getAllList() {
        itemsModelListFiltered = stateMains;
        notifyDataSetChanged();
    }

    public int getDelList(String kodtov) {
        int i = 0;
        for(StateMain itemsModel: stateMains){
            if (itemsModel.getKodtov().equals(kodtov)) {
                return i;
            }
            i++;
        }
        return i;
    }

    public Filter getFilterDel() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence kodtov) {
                FilterResults filterResults = new FilterResults();
                List<StateMain> resultsModel = new ArrayList<>();

                if((kodtov == null || kodtov.length() == 0)){
                    filterResults.count = stateMains.size();
                    filterResults.values = stateMains;
                }else{
                    for(StateMain itemsModel: stateMains){
                        if (!itemsModel.getKodtov().equals(kodtov)){
                            resultsModel.add(itemsModel);
                        }else{
                        }
                        filterResults.count = resultsModel.size();
                        filterResults.values = resultsModel;
                    }
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemsModelListFiltered = (List<StateMain>) results.values;
                stateMains.clear();
                stateMains = itemsModelListFiltered;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    public void setValueFact(String st, int position) {
        StateMain stateMain = itemsModelListFiltered.get(position);
        stateMain.setKolvo(st);
        itemsModelListFiltered = stateMains;
    }
}
