package ru.an_sklad.ru.ui.place_adv;

import android.view.View;

public class State {

    private String vCode; // артикул
    private String place; // место хранения
    private String barcode1; // ШК МХ
    private String recId; // rec_id
    private String countF; // количество по факту
    private String countB; // количество по факту
    private String vName; // название позиции
    private String comment; // комментарий
    private String kodTov; // код товара

    public State(String textViewVCode, String textViewPlace, String textViewBarcode1,
                 String textViewRecId, String editTextCountF, String textViewVName,
                 String editTextComment, String textViewKodTov, String textViewCountBase){
        this.vCode=textViewVCode;
        this.place=textViewPlace;
        this.barcode1=textViewBarcode1;
        this.recId=textViewRecId;
        this.countF=editTextCountF;
        this.vName=textViewVName;
        this.comment=editTextComment;
        this.kodTov=textViewKodTov;
        this.countB=textViewCountBase;
    }

    public String getVName() {
        return this.vName;
    }

    public String getvCode() {
        return this.vCode;
    }


    public String getPlace() {
        return this.place;
    }

    public String getRecId() {
        return this.recId;
    }

    public String getBarcode1() {
        return this.barcode1;
    }

    public String getcountF() {
        return this.countF;
    }
    public String getcountB() {
        return this.countB;
    }

    public String getcomment() {
        return this.comment;
    }
    public String getKodTov() {
        return this.kodTov;
    }

    public void setVCode(String vCode) {
        this.vCode = vCode;
    }
    public void setVName(String vName) {
        this.vName = vName;
    }
    public void setPlace(String place) {
        this.place = place;
    }
    public void setBarcode1(String barcode1) { this.barcode1 = barcode1; }
    public void setRecId(String recId) { this.recId = recId; }
    public void setcountF(String countF) { this.countF = countF; }
    public void setcountB(String countB) { this.countB = countB; }
    public void setcomment(String comment) { this.comment = comment; }
    public void setKodTov(String kodTov) { this.kodTov = kodTov; }
}