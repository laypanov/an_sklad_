package ru.an_sklad.ru.ui.shelfcheck2;

public class State {

    private String shortNamex; // название полки
    private String user1; // пользователь1
    private String user2; // пользователь2
    private String timestamp; // дата
    private String kolvoBase; // кол-во бд
    private String kolvoFact; // кол-во факт
    private String employFullName; // ФИО
    private String countNowData; // ФИО

    public State(String textViewShelfName, String user1, String user2, String textViewDateData,
                 String TextViewCountBaseData, String TextViewCountFactData, String employFullName){

        this.shortNamex=textViewShelfName;
        this.user1=user1;
        this.user2=user2;
        this.timestamp=textViewDateData;
        this.kolvoBase=TextViewCountBaseData;
        this.kolvoFact=TextViewCountFactData;
        this.employFullName=employFullName;
    }

    public String getShortNamex() {
        return this.shortNamex;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public String getKolvoBase() {
        return this.kolvoBase;
    }

    public String getkolvoFact() { return this.kolvoFact; }

    public String getEmployFullName() { return this.employFullName; }

    public String getCountNowData() { return this.countNowData; }

    public void setShortNamex(String shortNamex) {
        this.shortNamex = shortNamex;
    }
    public void setTimestamp(String timestamp) { this.timestamp = timestamp; }
    public void setKolvoBase(String kolvoBase) { this.kolvoBase = kolvoBase; }
    public void setKolvoFact(String kolvoFact) { this.kolvoFact = kolvoFact; }
    public void setEmployFullName(String employFullName) { this.employFullName = employFullName; }
    public void setCountNowData(String countNowData) { this.countNowData = countNowData; }
}