package ru.an_sklad.ru.ui.scn;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.List;

import ru.an_sklad.ru.R;

public class ListStateAdapter extends  ArrayAdapter<State> implements Filterable {

    private LayoutInflater inflater;
    private int layout;
    private List<State> states;
    private List<State> itemsModelListFiltered;

    public ListStateAdapter(Context context, int resource, List<State> states) {
        super(context, resource, states);
        this.states = states;
        this.itemsModelListFiltered = states;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itemsModelListFiltered.size();
    }

    @Override
    public State getItem(int position) {
        return itemsModelListFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=this.inflater.inflate(this.layout, parent, false);

        TextView scankod = view.findViewById(R.id.textViewScanKod);
        TextView placeName = view.findViewById(R.id.textViewPlaceName);
        TextView kolvo = view.findViewById(R.id.textViewKolvo);
        TextView nameTov = view.findViewById(R.id.textViewNameTov);
        TextView zonaId = view.findViewById(R.id.textViewZonaVal);
        TextView mesto_id = view.findViewById(R.id.textViewMestoId);

        State state = itemsModelListFiltered.get(position);

        scankod.setText(state.getScankod());
        placeName.setText(state.getPlaceName());
        kolvo.setText(state.getKolvo());
        nameTov.setText(state.getNameTov());
        zonaId.setText(state.getZonaId());
        mesto_id.setText(state.getMestoId());

        return view;
    }

    public void clearList() {
        states.clear();
        itemsModelListFiltered.clear();
        notifyDataSetChanged();
    }

    public void getAllList() {
        itemsModelListFiltered = states;
        notifyDataSetChanged();
    }
}
